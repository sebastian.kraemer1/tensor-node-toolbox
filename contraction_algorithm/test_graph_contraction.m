% function test_graph_contraction()

Modes = {'optimal','greedy_use_xy','greedy_use_cost','greedy_use_xy/c0.5'};
mode = Modes{1};

V_ghost = [];

%% random graph
% n_nodes = 8;
% n_edges = 15;
% G = struct;
% G.Edge = cell(n_edges,1);
% G.Edge_Size = zeros(n_edges,1);
% 
% for i = 1:n_edges
%     % new random edge
%     n = randi(3);
%     p = randperm(n_nodes);
%     edge = p(1:n);
%     G.Edge{i} = edge;
%     
%     G.Edge_Size(i) = 1+randi(10); 
% end

%% simple graph
n_nodes = 5;
n_edges = 6;
G.Edge = {1; [1,2]; [1,3]; [2,3,4]; [3,4]; [4,5]};
G.Edge_Size = [2,3,4,5,6,7];

%% TT contraction
% d = 2;
% n_nodes = 4*d;
% n_edges = 4*(d-1) + 3*d;
% G.Edge = cell(n_edges,1);
% G.Edge_Size = zeros(1,n_edges);
% k = 1;
% 
% for i = 1:d-1
%     for j = 0:3
%         G.Edge_Size(k) = 3;
%         G.Edge{k} = [i+d*j,i+d*j + 1]; k = k + 1;
%     end
% end
% 
% for i = 1:d
%     for j = 0:2
%         G.Edge_Size(k) = 100;
%         G.Edge{k} = [i+d*j,i+d*j + d]; k = k + 1;
%     end
% end
% 
% if d == 3
%     V_ghost = [2,11];
% end

view_result = 1;
graph_contraction3(G,n_nodes,V_ghost,mode,view_result)