function [Is,Js,Name_Part,V] = graph_contraction3(G,n_nodes,V_ghost,mode,view_result,optfac)
iter_max = 30000;

if nargin == 4
    view_result = 0;
end

n_edges = length(G.Edge);

%% mode
Modes = {'optimal','greedy_use_cost','greedy_use_xy','greedy_use_xy/c0.5'};

if ~any(strcmp(mode,Modes))
    error('Unknown mode %s\n',mode);
end


%% main
Size = ones(n_nodes,1);
Node_to_Edge = false(n_nodes,n_edges);
Shared_Size = ones(n_nodes,n_nodes);
for i = 1:n_edges
    Node_to_Edge(G.Edge{i},i) = true;
    v = Node_to_Edge(:,i);
    
    Shared_Size(v,v) = Shared_Size(v,v)*G.Edge_Size(i);
    Size(v) = Size(v)*G.Edge_Size(i);
end

Contraction_Cost = calc_cost(Size,Shared_Size);
% the greedy_use_xy rating does not work well for mode sizes that are
% involved in more than 2 nodes. 
Rating = calc_rating(Contraction_Cost,Size,Shared_Size,mode);
Rating(V_ghost,:) = Inf;
Rating(:,V_ghost) = Inf;


[CC_,R_,R_block_prev,G_,Size_,Shared_Size_,Name_Part_] = deal(cell(iter_max,1));
Node_to_Edge_ = false(n_nodes,n_edges,iter_max);


CC_{1} = Contraction_Cost;
R_{1} = Rating;
R_block_prev{1} = R_{1};
G_{1} = G;
Node_to_Edge_(:,:,1) = Node_to_Edge;
Size_{1} = Size;
Shared_Size_{1} = Shared_Size;

K = 1;

min_CC = zeros(iter_max,1);
[I_last,I,J_last,J] = deal(zeros(iter_max,1));
 
cost_last = zeros(iter_max,1);
accum_cost = Inf*ones(iter_max,1);
pred = zeros(1,iter_max);

[I(1),J(1),v] = matrix_min(R_{1}); 
if v == Inf
    mode = 'mul_unconnected';
    R_{1} = calc_rating(CC_{1},Size_{1},Shared_Size_{1},mode);
    R_{1}(V_ghost,:) = Inf;
    R_{1}(:,V_ghost) = Inf;
    R_block_prev{1} = R_{1};
    [I(1),J(1),v] = matrix_min(R_block_prev{1});
end
    
min_CC(1) = CC_{1}(I(1),J(1));

accum_cost(1) = 0; 
edge = zeros(iter_max,2);
edge_label = cell(iter_max,1);
node_label = cell(iter_max,1);
node_label{1} = sprintf('(%d) %d',1,0);
K_max_plot_size = 80;

rate_f = @(a,b) a+b; % optimal in order of complexity: max([a,b],[],2);

K_max = 1;
lvl = zeros(iter_max,1);
edge_nr = 0;

while K_max <= iter_max 
    % current status: K
    % current maximal number of stati: K_max
 
    % contraction choice of [I,J] leads to new status K_max:
    [CC_{K_max+1},R_{K_max+1},G_{K_max+1},Node_to_Edge_(:,:,K_max+1),Size_{K_max+1},Shared_Size_{K_max+1},Name_Part_{K_max+1}] = update_Contraction_cost(CC_{K},R_{K},G_{K},Node_to_Edge_(:,:,K),Size_{K},Shared_Size_{K},I(K),J(K),mode,V_ghost);
    
    % save nr of contractions (depths within tree)
    lvl(K_max+1) = lvl(K) + 1;
    
    % CC_ is the one to construct the succ. cost function, cant use the
    % blocked one
    R_block_prev{K_max+1} = R_{K_max+1};

    % save the choice over which status K_max was reached from status K
    I_last(K_max+1) = I(K);
    J_last(K_max+1) = J(K);
    cost_last(K_max+1) = min_CC(K);
    
    % accumulate cost (already ex. cost + cost to get to K_max+1)
    accum_cost(K_max+1) = rate_f(cost_last(K_max+1),accum_cost(K));
    
    %% In case all contractions are supposed to be connected:
    % choice = (1:n_nodes~=I_last);
    % R_block_prev{K_max+1}(choice,choice) = Inf;
    %%
    
    % prevent taking already taken choice I(K),J(K)
    R_block_prev{K}(I(K),J(K)) = Inf; 
    
    % save next choice and its cost from pred. status K (+prohibits)
    [I(K),J(K),v1] = matrix_min(R_block_prev{K}); % rating
    if v1 == Inf
        min_CC(K) = Inf;
    else
        min_CC(K) = CC_{K}(I(K),J(K)); % cost
    end
    
    % compare if we have constructed a graph which was already considered
    % (and did not have higher cost)
    same_lvl = find(lvl(1:K_max)==lvl(K_max+1));
    cycle_nr = same_lvl(sum(sum(abs(Node_to_Edge_(:,:,same_lvl) - repmat(Node_to_Edge_(:,:,K_max+1),[1,1,length(same_lvl)])),1),2)==0);
    cheaper_order_or_new_path = all(accum_cost(K_max+1) < accum_cost(cycle_nr));
    
    if cheaper_order_or_new_path

        % remember pred. status
        pred(K_max+1) = K;
        
        % find new greedy option in status K_max+1 (without those being blocked)
        [I(K_max+1),J(K_max+1),v2] = matrix_min(R_block_prev{K_max+1}); % rating
        if v2 == Inf && lvl(K_max+1) < n_nodes-1-length(V_ghost)
            % Multiply remaining unconnected parts (mode size 1 does not count as connection)
            % Greedy cost multiplication is optimal from here on
            mode = 'mul_unconnected';
            R_{K_max+1} = calc_rating(CC_{K_max+1},Size_{K_max+1},Shared_Size_{K_max+1},mode);
            R_{K_max+1}(V_ghost,:) = Inf;
            R_{K_max+1}(:,V_ghost) = Inf;
            R_block_prev{K_max+1} = R_{K_max+1};
            [I(K_max+1),J(K_max+1),v2] = matrix_min(R_block_prev{K_max+1});
        end            
            
        min_CC(K_max+1) = CC_{K_max+1}(I(K_max+1),J(K_max+1)); % cost
        
        K_max = K_max + 1;
         
        % for plot
        edge_nr = edge_nr + 1; edge(edge_nr,:) = [K,K_max];
        node_label{K_max} = sprintf('[%d,%d] (%d) -%d-> %d',I_last(K_max),J_last(K_max),K_max,cost_last(K_max),accum_cost(K_max));           
%         PG = graph(edge(1:edge_nr,1),edge(1:edge_nr,2));
%         h = plot(PG,'NodeLabel',node_label(1:K_max));

        % all connected edges have been contracted
        if v2 == Inf
            break;
        end        
    end

    if strcmp(mode,'optimal')
        rf = rate_f(accum_cost(1:K_max),min_CC(1:K_max));
        % Pseudo Optimality: 
        rf(K_max) = optfac*rf(K_max);
        
        v = min(rf);
        K = find(rf==v,1,'last');
    else
        K = K_max;
    end
    
end



if K_max > iter_max
    error('Maximal number of iterations (%d) reached before result was found',iter_max);
end

% if v < Inf
% find path through contractions (backwards)
depth = lvl(K_max);
p = zeros(depth,1);
p(depth) = K_max;

% Is = zeros(depth,1);
% Js = zeros(depth,1);
for i = depth:-1:1
%     Is(i) = I_last(p(i));
%     Js(i) = J_last(p(i));
    if i > 1
        p(i-1) = pred(p(i));
    end
end
Is = I_last(p);
Js = J_last(p);
Name_Part = Name_Part_(p);

% set of unconnected nodes indices (for a connected network V equals [1])
V = int_setdiff(1:n_nodes,Js');
% else
%     V = 1:n_nodes;
% end

V = int_setdiff(V,V_ghost);

if ~length(V)==1
    error('Not all modes have been multiplied \n');
end
    
if view_result
    PG = graph(edge(1:edge_nr,1),edge(1:edge_nr,2));
    h = plot(PG,'NodeLabel',node_label(1:K_max));
    
    fprintf('\nAccum cost: %d, Iterations: %d \n',accum_cost(K_max),K_max);
    fprintf('Contractions:\n')
    fprintf('[%2d <- %2d] - %10d -> %10d \n',[Is,Js,cost_last(p),accum_cost(p)]')
    fprintf('Indices of nodes remaining: ')
    fprintf('%d ',V)
    fprintf('\n\n')
end

end

function [I,J,min_val] = matrix_min(A)
[min_per_col,I] = min(A);
[min_val,j] = min(min_per_col);
ind = sort([I(j),j]);
I = ind(1); J = ind(2);
end

function [Contraction_Cost,Rating,G,Node_to_Edge,Size,Shared_Size,Name_Part] = update_Contraction_cost(Contraction_Cost,Rating,G,Node_to_Edge,Size,Shared_Size,I,J,mode,V_ghost)
[n_nodes,~] = size(Node_to_Edge);

common_edges = find(Node_to_Edge(I,:) & Node_to_Edge(J,:));
remove_nr = sum(Node_to_Edge(:,common_edges),1)<3;
remove = common_edges(remove_nr);
keep = common_edges(~remove_nr);
namesI = find(Node_to_Edge(I,:) & ~Node_to_Edge(J,:));
namesJ = find(Node_to_Edge(J,:) & ~Node_to_Edge(I,:));

Name_Part = struct('remove',remove,'keep',keep,'namesI',namesI,'namesJ',namesJ);

accum_edges = Node_to_Edge(I,:) | Node_to_Edge(J,:);
for i = find(accum_edges)
    v = Node_to_Edge(:,i);
    Shared_Size(v,v) = Shared_Size(v,v)/G.Edge_Size(i);
    Size(v) = Size(v)/G.Edge_Size(i);
end

Node_to_Edge(I,:) = accum_edges;
Node_to_Edge(J,:) = false;
Node_to_Edge(:,remove) = false;

tn = false(n_nodes,1); % touched nodes
for i = find(accum_edges)
    v = Node_to_Edge(:,i);
    tn = tn | v;

    Shared_Size(v,v) = Shared_Size(v,v)*G.Edge_Size(i);
    Size(v) = Size(v)*G.Edge_Size(i);
end

Contraction_Cost(tn,tn) = calc_cost(Size(tn),Shared_Size(tn,tn));
Rating(tn,tn) = calc_rating(Contraction_Cost(tn,tn),Size(tn),Shared_Size(tn,tn),mode);

Contraction_Cost(J,:) = NaN; % node J no longer has any meaning
Contraction_Cost(:,J) = NaN;
Rating(J,:) = Inf;
Rating(:,J) = Inf;
Rating(V_ghost,:) = Inf;
Rating(:,V_ghost) = Inf;

end

function Cost = calc_cost(Size,Shared_Size)
    Cost = Size*Size' ./ Shared_Size;
%     Cost = Cost + tril(Inf(size(Cost))); (not necessary)
%     Cost(Shared_Size == 1) = Inf; 
end

function Rating = calc_rating(Cost,Size,Shared_Size,mode)
% --x--[node]--c--[node]--y--  (x,c,y: mode size products)
% Cost: xcy
% Size: xc, Size': cy
% Shared_Size: c

if strcmp(mode,'greedy_use_xy')
    S = repmat(Size,[1,length(Size)]);
    Rating = Cost ./ ( (S + S') ); % xcy / (xc + yc)
    
elseif strcmp(mode,'greedy_use_xy/c')
    S = repmat(Size,[1,length(Size)]);
    Rating = Cost ./ ( (S + S') ) ./ Shared_Size.^(0.5); % xcy / (xc + yc) / c^(0.5)
 
else % if strcmp(mode,'greedy_use_cost') || strcmp(mode,'optimal') || strcmp(mode,'mul_unconnected')    
    Rating = Cost;

end

Rating = Rating + tril(Inf(size(Cost)));

if ~strcmp(mode,'mul_unconnected')
    Rating(Shared_Size == 1) = Inf;
end


end















