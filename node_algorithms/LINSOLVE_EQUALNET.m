function N = LINSOLVE_EQUALNET(A,N,T,iter_max,r,G)
% LINSOLVE_EQUALNET  solves boxtimes(A,N) = T for equal networks A,N,T
%
%   LINSOLVE_EQUALNET(A,N,T,iter_max) performs alternating least squares
%   for fixed rank to minimize ||boxtimes(A,N) - T||_F for N.
%
%   The algorithm makes use of the equal structure of networks and computes
%   the normal equations branch-wise.
%
%   See also: TENSOR_NODE_NOTATION10_ALTERNATING_OPTIMIZATION.mlx,
%   LINSOLVE, ORTHO, PATHQR

%% input
if nargin <= 4
    r = 1;
end
if nargin <= 5
    G = net_derive_G(N);
end

%% apply unique identifiers (and later use [N_prime{:}] = derive_net(prime_rn,N); )
nF = length(T);
nA = length(A);
nN = length(N);

% rename mode names in main equation AN = M
Net = {T,{A,N}};
[Z,glob_repl,outer_mn_id,seed] = get_mn_replacements(Net);
[Z,~,renaming1] = rename_mn(Z,glob_repl,outer_mn_id);
T = Z(1:nF);
A = Z(nF+1:nF+nA);
N = Z(nF+nA+1:end);

% derive nodes in AN with different inner mode names
Net = {A,N};
[Z,glob_repl,outer_mn_id] = get_mn_replacements(Net,seed);
[Z,~,~,prime_rn] = rename_mn(Z,glob_repl,outer_mn_id);
A_prime = Z(1:nA);
N_prime = Z(nA+1:end);

%% branch-wise products
Br = branches(G,r);
Br_prod_ANTAN_towards_root = cell(1,numnodes(G));
Br_prod_ANTAN_away_from_root = cell(1,numnodes(G));
Br_prod_ANTM_towards_root = cell(1,numnodes(G));
Br_prod_ANTM_away_from_root = cell(1,numnodes(G));

Br_prod_ANTAN_towards_root{r} = [];
Br_prod_ANTM_towards_root{r} = [];

%% M^T M
MTM_node = boxtimes(T,T);
MTM = MTM_node.data;

%% Pre-comnputations
is_iter_zero = true; % ortho to r and record Br_prod 2, dont optimize
ALSREC(r,[]);
is_iter_zero = false;

for iter = 1:iter_max
    ALSREC(r,[]);
    
    ANTAN = Br_prod_ANTAN_away_from_root{r}.data;
    ANTM = Br_prod_ANTM_away_from_root{r}.data;
    relres = sqrt(ANTAN - 2*ANTM + MTM)/sqrt(MTM);
    fprintf('rel res: %.2e\n',relres);
end

[N{:}] = derive_net(renaming1,N{:});

    function ALSREC(b,P)
        Neighb = neighbors(G,b)';
        
        for h = setdiff(neighbors(G,b)',P)
            
            if ~is_iter_zero
                % root to leaf QR (N is then h-orthogonal)
                [N,~,path] = PATHQR(N,b,h,G);  % only changes b and h
                [N_prime{path}] = derive_net(prime_rn,N{path});
                               
                % Br_prod_ANTAN_towards_root{b}
                U = int_setdiff(neighbors(G,b)',[P,h]);
                
                % combine branches
                MulNet = [{}, Br_prod_ANTAN_towards_root{b}, A{b}, A_prime{b}, N(b), N_prime(b), Br_prod_ANTAN_away_from_root{U}];
                
                Br_prod_ANTAN_towards_root{h} = boxtimes(MulNet{:},'mode','optimal');
                
                MulNet = [{}, Br_prod_ANTM_towards_root{b}, T{b}, A_prime{b}, N_prime(b), Br_prod_ANTM_away_from_root{U}];
                
                Br_prod_ANTM_towards_root{h} = boxtimes(MulNet{:},'mode','optimal');
            end
                            
            % step towards leaf
            ALSREC(h,b) % last updated N_h
        
        end
        
        H = int_setdiff(Neighb,P);
        
        % solve least squares problem in node b via normal equation
        if ~is_iter_zero                
            % (AN w/o N_b)'*(AN w/o N_b):
            MulNet = [{}, A{b}, A_prime{b}, Br_prod_ANTAN_towards_root{b}, Br_prod_ANTAN_away_from_root{H}];
            ANwoN_bTANwoN_b = boxtimes(MulNet{:},'mode','optimal');
            
            % (AN w/o N_b)'*T 
            MulNet = [{}, T{b}, A_prime{b}, Br_prod_ANTM_towards_root{b}, Br_prod_ANTM_away_from_root{H}];
            ANwoN_bTM = boxtimes(MulNet{:},'mode','optimal');
            
            % new N_b
            N{b} = node_linsolve(ANwoN_bTANwoN_b,ANwoN_bTM);
            N_prime{b} = derive_net(prime_rn,N{b});
        end
        
        if ~isempty(P)
            % leaf to root QR (N is then P-orthogonal)
            [N,~,path] = PATHQR(N,b,P,G); % only changes b and P
            [N_prime{path}] = derive_net(prime_rn,N{path});
        end
        
        % ghost nodes are not necessary for plain networks
        MulNet = [{}, A{b}, A_prime{b}, N(b), N_prime(b), Br_prod_ANTAN_away_from_root{H}];
        
        Br_prod_ANTAN_away_from_root{b} = boxtimes(MulNet{:},'mode','optimal');
        
        MulNet = [{}, A_prime{b}, N_prime(b), Br_prod_ANTM_away_from_root{H}, T{b}];
        
        Br_prod_ANTM_away_from_root{b} = boxtimes(MulNet{:},'mode','optimal');
        
    end

end