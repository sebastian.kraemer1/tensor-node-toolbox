function [N,fN,sigma,sigma_map] = STAND(N,r,G)
% STAND  standardizes a plain network
%
%   [N,fN,sigma] = STAND(N,r) returns the standard representation (fN,sigma) 
%   of the tensor represented by N. The output N is the orthogonal with
%   respect to r.
%
%   The input N must be a plain network.
%
%   sigma will contain the singular values of the different matricizations
%   corresponding to the network. 
%
%   It holds boxtimes(N{:}) = boxtimes(fN{:},sigma{:}).
%
%   See also: TENSOR_NODE_NOTATION8_STANDARD_REPRESENTATIONS.mlx, ORTHO

if nargin <= 1
    r = 1;
end

if nargin <= 2
    G = net_derive_G(N);
end

k = numnodes(G);
fN = cell(1,k);
sigma = cell(1,k-1);
N = ORTHO(N,r,G);
STANDREC(r,[]);

    function Q = STANDREC(b,P)
        Nbtilde = N{b};
        for h = int_setdiff(neighbors(G,b)',P)
            gamma = str_intersect(N{h}.mode_names,N{b}.mode_names);
            [~,R] = node_qr(N{b},gamma);
            N{h} = boxtimes(R,N{h});
            Q = STANDREC(h,b);
            Nbtilde = boxtimes(Nbtilde,matrix_node_pinv(R));
            Nbtilde = boxtimes(boxtimes_part(sigma{findedge(G,h,b)},Q,{},gamma),Nbtilde);             
        end  
        N{b} = Nbtilde;
            
        if ~isempty(P)  
            gamma = str_intersect(N{P}.mode_names,N{b}.mode_names);
            [U,s,Q] = node_svd(N{b},listdiff(N{b}.mode_names,gamma),gamma,0);
            N{b} = U;
            fN{b} = U;
            
            ind = findedge(G,P,b);
            sigma{ind} = s;
            sigma_map.(gamma{1}) = ind;
        else
            fN{b} = N{b};
        end
        
        for h = int_setdiff(neighbors(G,b)',P)
            s = sigma{findedge(G,h,b)};
            s.data = (s.data).^(-1);
            fN{b} = boxtimes_part(s,fN{b},{},s.mode_names);
        end  
    end
end