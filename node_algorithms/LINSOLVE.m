function N = LINSOLVE(A,N,T,iter_max,r,G)
% LINSOLVE_EQUALNET  solves boxtimes(A,N) = T for arbitrary networks A,N,T
%
%   LINSOLVE_EQUALNET(A,N,T,iter_max) performs alternating least squares
%   for fixed rank to minimize ||boxtimes(A,N) - T||_F for N.
%
%   Should not be used for too large networks.
%
%   See also: TENSOR_NODE_NOTATION10_ALTERNATING_OPTIMIZATION.mlx,
%   LINSOLVE_EQUALNET, ORTHO, PATHQR

if nargin <= 4
    r = 1;
end

if nargin <= 5
    G = net_derive_G(N);
end

N = ORTHO(N,r);
h_crt = r;

for iter = 1:iter_max
    ALSREC(r,[]);
end

    function ALSREC(b,P)
        for h = setdiff(neighbors(G,b)',P)
            ALSREC(h,b)           
        end
        
        [N,h_crt] = PATHQR(N,h_crt,b,G); % for stability
        
        % solve for N(b): boxtimes(A,[N(all_but_b),N(b)]) = T
        all_but_b = ~(1:numel(N)==b);
        
        H = boxtimes({A,N(all_but_b),ghost(N{b})},{A,N(all_but_b),ghost(N{b})});
        B = boxtimes({A,N(all_but_b),ghost(N{b})},T); 
        
        N{b} = node_linsolve(H,B);
    end

end