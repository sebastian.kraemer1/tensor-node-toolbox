function N = ALS(N,T,iter_max,r,G)
% ALS  is a Vanilla implementation of alternating least squares
%
%   N = ALS(N,T,iter_max) performs iter_max steps of alternating least
%   squares in order to minimize \|N - T\|. 
%
%   It solely uses orthogonalizations in order to simplify the calculation.
%
%   See also: TENSOR_NODE_NOTATION10_ALTERNATING_OPTIMIZATION.mlx, ORTHO,
%   PATHQR, LINSOLVE

if nargin <= 3
    r = 1;
end

if nargin <= 4
    G = net_derive_G(N);
end

N = ORTHO(N,r);
h_crt = r;

for iter = 1:iter_max
    ALSREC(r,[]);
end

    function ALSREC(b,P)
        for h = setdiff(neighbors(G,b)',P)
            ALSREC(h,b)           
        end
        
        [N,h_crt] = PATHQR(N,h_crt,b,G);
        
        all_but_b = true(size(N));
        all_but_b(b) = false;
        N{b} = boxtimes(N(all_but_b),T);   
    end

end