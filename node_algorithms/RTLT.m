function [N,Tprime] = RTLT(T,m,r,G,M,tol)
% RTLT  Root-to-leaves truncation of a tensor.
%
%   Input:
%       T: tensor node
%       m: a cell for which m{k} are the mode names of node number k
%          (the corresponding network must be plain)
%       r: a root node
%
%   -optional:
%       G: [] or the graph equal to derive_G(m)
%       M: [] or the cell equal to derive_M(G,m,r)
%       tol: tolerance for singular value decompositions
%
%   Output:
%       N: a tensor network representing the root-to-leaves truncation of T
%       Tprime: equals boxtimes(N{:})
%
%   Example: 
%       alpha = mna('alpha',1:6);
%       n = assign_mode_size(alpha,1+(1:6));
%       T = init_node(alpha,n);
%        
%       T = randomize_node(T);
%       node_size(T)
%        
%       S = binary_tree(alpha);
%       [G,m,M,r] = RTLGRAPH(alpha,S);
% 
%       [N,Tprime] = RTLT(T,m,r); % = RTLT(T,m,r,G,M,1e-14);
%       data_volume(N)
%       data_volume(T)
%       view(G,m,N);
%         
%       norm(unfold(Tprime,alpha) - unfold(T,alpha))
%       norm(unfold(Tprime,alpha) - unfold(boxtimes(N{:}),alpha))
%         
%       boxtimes(N,N);
%       norm(T.data(:))
%
%   See also: TENSOR_NODE_NOTATION9_DECOMPOSITIONS.mlx, RTLGRAPH, NODE_SVD,
%       NODE_QR, BOXTIMES, UNFOLD, DATA_VOLUME

if nargin == 3 || isempty(G)
    G = derive_G(m);
end
if nargin <= 4 || isempty(M)
    M = derive_M(G,m,r);
end
if nargin <= 5
    tol = 1e-14;
end

k = numnodes(G); % expects V = {1,...,k}
N = cell(1,k);

output_Tprime = false;
if nargout == 2
    output_Tprime = true;
    Tprime = T;
end

RTLTREC(r,[]);

    function RTLTREC(b,P)
        if ~isempty(P)
            gamma = str_intersect(m{b},m{P});
            delta = M{b};
            [U,~,~] = node_svd(T,delta,gamma,tol);
            N{b} = U;
            N{P} = boxtimes(N{P},U); 
            
            if output_Tprime
                Tprime = boxtimes(boxtimes(Tprime,U),U);
            end
        else
            N{b} = T;
        end
        
        for h = int_setdiff(neighbors(G,b)',P)
            RTLTREC(h,b);
        end
    end
end