function N = ORTHO(N,r,G)
% ORTHO  Orthogonalization with respect to node r of a tree network
%
%   N = ORTHO(N,r) orthogonalizes the network N with respect to the root
%   r. 
%
%   N must be a cell containing the tensor nodes of the network. Each node
%   must not have dublicate mode names. Every mode name must further appear
%   in at most two nodes (we call this a plain network).
%
%   N = ORTHO(N,r,G) expects G to be the same as net_derive_G(N) and does
%   hence not need to calculate the graph G corresponding to the tree
%   network.
%
%     See also: TENSOR_NODE_NOTATION5_NODE_QR_AND_NODE_SVD.mlx, PATHQR,
%     NET_DERIVE_G

if nargin <= 2
    G = net_derive_G(N);
end

ORTHOREC(r,[]);

    function ORTHOREC(b,P)
        for h = int_setdiff(neighbors(G,b)',P)
            ORTHOREC(h,b);
        end
        
        if ~isempty(P)
            gamma = str_intersect(N{P}.mode_names,N{b}.mode_names);
            [Q,R] = node_qr(N{b},gamma);
            N{b} = Q;
            N{P} = boxtimes(R,N{P});
        end
    end
end