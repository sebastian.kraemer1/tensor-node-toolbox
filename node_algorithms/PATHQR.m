function [N,t,P] = PATHQR(N,s,t,G)
% PATHQR  changes the orthogonalization of a plain network
%
%   PATHQR(N,s,t) takes a network orthogonalized to s and changes the
%   orthogonalization to t. The network must be plain, for example the
%   output of ORTHO(N,s).
%
%   See also: TENSOR_NODE_NOTATION5_NODE_QR_AND_NODE_SVD.mlx, ORTHO

if nargin <= 3
    G = net_derive_G(N);
end

P = shortestpath(G,s,t);

for i = 1:length(P)-1
    b = P(i); h = P(i+1);
    gamma = str_intersect(N{b}.mode_names,N{h}.mode_names);
    [Q,R] = node_qr(N{b},gamma);
    N{b} = Q;
    N{h} = boxtimes(R,N{h});
end