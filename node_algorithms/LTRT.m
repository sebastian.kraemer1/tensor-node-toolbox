function [N,fN,sigma] = LTRT(T,m,r,G,tol)
% LTRT  Leaves-to-root truncation of a tensor.
%
%   Input:
%       T: tensor node
%       m: a cell for which m{k} are the mode names of node number k
%          (the corresponding network must be plain)
%       r: a root node
%
%   -optional:
%       G: [] or the graph equal to derive_G(m)
%       tol: tolerance for singular value decompositions
%
%   Output:
%       N: a tensor network representing the leaves-to-root truncation of T
%       fN/sigma: the standard representation of N, 
%       where boxtimes(N{:}) = boxtimes(fN{:},sigma{:})
%
%   Example: 
%       alpha = mna('alpha',1:6);
%       n = assign_mode_size(alpha,1+(1:6));
%       T = init_node(alpha,n);
% 
%       T = randomize_node(T);
%       node_size(T)
% 
%       S = binary_tree(alpha);
%       [G,m,M,r] = RTLGRAPH(alpha,S);
% 
%       [N,fN,sigma] = LTRT(T,m,r); % = LTRT(T,m,r,G,1e-14);
%       data_volume(N)
%       data_volume(T)
% 
%       T_trunc = boxtimes(N{:});
%       norm(unfold(T_trunc,alpha) - unfold(T,alpha))
%       norm(unfold(T_trunc,alpha) - unfold(boxtimes(fN{:},sigma{:}),alpha))
% 
%       check_if_standard(fN,sigma,G)
%
%   See also: TENSOR_NODE_NOTATION9_DECOMPOSITIONS, RTLT, NODE_SVD,
%   NODE_QR, BOXTIMES, UNFOLD, DATA_VOLUME

if nargin == 3 || isempty(G)
    G = derive_G(m);
end
if nargin <= 4
    tol = 1e-14;
end

k = numnodes(G);
N = cell(1,k);
fN = cell(1,k);
sigma = cell(1,k-1);
DECOMPREC(r,[]);

    function s = DECOMPREC(b,P)
        for h = int_setdiff(neighbors(G,b)',P)
            DECOMPREC(h,b);
        end
        
        if ~isempty(P)
            gamma = str_intersect(m{P},m{b});
            [U,s,Q] = node_svd(T,listdiff(m{b},gamma),gamma,tol);
            T = boxtimes_part(s,Q,{},gamma); % = boxtimes(node_diag(s),Q);
            N{b} = U;
            fN{b} = U;
            sigma{findedge(G,P,b)} = s;
        else
            N{b} = T;
            fN{b} = T;
        end
        
        for h = int_setdiff(neighbors(G,b)',P)
            s = sigma{findedge(G,h,b)};
            s.data = (s.data).^(-1);
            fN{b} = boxtimes_part(s,fN{b},{},s.mode_names);
        end            
    end

end