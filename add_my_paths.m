function add_my_paths()

% addpath(genpath('/home/data/kraemer/Promotion/Matlab Toolboxes'));% for multiprod
addpath(genpath('graph_algorithms'));
addpath(genpath('tensor_algorithms'));
addpath(genpath('graph_related'));
addpath(genpath('node_algorithms'));
addpath(genpath('node_related'));
addpath(genpath('misc'));
addpath(genpath('tensor_node_live_scripts'));
addpath(genpath('contraction_algorithm'));