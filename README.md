# tensor node toolbox

For software copyright issues, see the LICENSE file.
Please note the copyright and citation issues implied by related publications.

NOTE:
The toolbox is fully functional, but some descriptions might be outdated and some parts may still be subject to changes and improvements.

HOW TO GET STARTED:
First download 'multiprod.m' written by Paolo de Leva (available on mathworks.com) for faster multiplication of arrays of matrices:
https://www.mathworks.com/matlabcentral/fileexchange/8773-multiple-matrix-multiplications-with-array-expansion-enabled

The live scripts contain a comprehensive introduction to the toolbox. They are also precompiled as pdfs.
Note that some of the worksheets require .mat files produced by earlier ones.
