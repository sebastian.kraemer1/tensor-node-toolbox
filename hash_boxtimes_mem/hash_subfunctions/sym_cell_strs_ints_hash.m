function hash_code = sym_cell_strs_ints_hash(Z1,Z2)

leb = size(Z1,2);
b = zeros(1,leb);
for i = 1:leb
    b(i) = str_int_hash(Z1{i},Z2(i));
end

hash_code = sym_ints_hash(b);




