function hash_code = str_hash(str)

hash_max = 100003; % is the same prime as in sym_double_hash

mul_256_ttpo_1_to_10 = [256 65536 76715 38452 43418 14675 56689 11949 58854 66174];
% ...(i) = powermod(255,i,100003)

b = double(str);

% b = powermod(b,23,hash_max); % this should not be neccessary

leb = length(b);
if leb <= 10
    hash_code = mod(mul_256_ttpo_1_to_10(1:leb)*b(:),hash_max);
else
    
    for i = 2:length(b)
        b(i:end) = mod(256*b(i:end),hash_max);
    end
    
    hash_code = mod(sum(b),hash_max);
end



