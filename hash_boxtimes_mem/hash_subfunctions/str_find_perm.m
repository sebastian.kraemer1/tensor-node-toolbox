function [pi,are_equal] = str_find_perm(b,a)
% finds pi such that a = b(pi)
are_equal = 0;

numela = numel(a);
pi = zeros(1,numel(a));

if numela ~= numel(b)
    return;
end

for j = 1:numel(a)
    cmp = find(strcmp(b{j},a),1);
    if isempty(cmp)
        return;
    end 
        
    a{cmp} = {};
    pi(j) = cmp;
end

are_equal = 1;

% function [pi,are_equal] = str_find_perm(b,a)
% % finds pi such that a = b(pi)
% are_equal = 0;
% pi = zeros(1,numel(a));
% 
% for j = 1:numel(a)
%     [it_is,pos] = ismember(b{j},a);
% %     pos = find(strcmp(b{j},a),1);
%     if ~it_is % isempty(pos)
%         return;
%     end 
%         
%     a{pos} = ' ';
%     pi(j) = pos;
% end
% 
% are_equal = 1;
