function hash_code = sym_strs_hash(varargin)

b = zeros(1,nargin);
for i = 1:length(varargin)
    b(i) = str_hash(varargin{i});
end

hash_code = sym_ints_hash(b);




