function hash_code = sym_ints_hash(b)

hash_max = 100003; % is the same prime as in str_hash

% b = powermod(b,23,100003);

pr = 1;
for i = 1:length(b)
    pr = mod(pr*b(i),hash_max);
end

hash_code = mod(pr+b(:)'*b(:),hash_max);