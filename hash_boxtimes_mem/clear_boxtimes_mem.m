function clear_boxtimes_mem(hash_list_length)
% CLEAR_BOXTIMES_MEM  clears the boxtimes memory haslist
%
%   CLEAR_BOXTIMES_MEM will neither activate nor deactivate the memory, but
%   clear all previous information.
%
%   See also: ACTIVATE_BOXTIMES_MEM


if nargin == 0
    hash_list_length = 2500;
end

global BOXTIMES_MEM

if exist('BOXTIMES_MEM','var') && isa(BOXTIMES_MEM,'struct') && isfield(BOXTIMES_MEM,'active')
    active_status = BOXTIMES_MEM.active;
else
    active_status = false;
end

BOXTIMES_MEM = struct;
BOXTIMES_MEM.active = active_status;
BOXTIMES_MEM.HASHLIST = -ones(1,hash_list_length);
BOXTIMES_MEM.mem = cell(1,hash_list_length);
BOXTIMES_MEM.hash_fill_nr = 0;
