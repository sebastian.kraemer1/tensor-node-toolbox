function deactivate_boxtimes_mem()
% DEACTIVATE_BOXTIMES_MEM  deactivates the memory function of boxtimes
%
%   See also: ACTIVATE_BOXTIMES_MEM

global BOXTIMES_MEM

if exist('BOXTIMES_MEM','var') && isa(BOXTIMES_MEM,'struct') % && isfield(BOXTIMES_MEM,'active') && BOXTIMES_MEM.active
    BOXTIMES_MEM.active = false;
end