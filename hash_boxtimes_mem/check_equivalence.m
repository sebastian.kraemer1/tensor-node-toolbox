function [perms,equivalent] = check_equivalence(Zall,Zall_ast,number_nodes)

equivalent = false;
perms = cell(1,number_nodes);

if length(Zall) ~= length(Zall_ast)
    return;
end

nr_node = 0;

for i = 1:length(Zall)
   if isa(Zall{i},'cell') && isa(Zall_ast{i},'cell')
        if size(Zall{i},1) == 1 && ~isempty(Zall{i}) % modification of multiplication through specified mode names (order invariant)
            [~,equal] = str_find_perm(Zall{i},Zall_ast{i});
            if ~equal
                return;
            end   
        else
            nr_node = nr_node + 1;
            if ~isempty(Zall{i})
                [perms{nr_node},equal] = str_find_perm(Zall_ast{i}{1},Zall{i}{1});
                if ~equal || any(Zall{i}{2}(perms{nr_node})-Zall_ast{i}{2})
                    return
                end
            end
        end
    else
        if ~isequal(Zall{i},Zall_ast{i})
            return;
        end
   end
end

equivalent = true;


end