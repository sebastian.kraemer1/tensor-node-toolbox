function hash_code = net_hash(Zall)

nZall1 = length(Zall);
Hash_code = zeros(1,nZall1);
for i = 1:nZall1
    if isa(Zall{i},'cell')
        if size(Zall{i},1) == 1
            Hash_code(i) = sym_strs_hash(Zall{i}{:});
        else
            Hash_code(i) = sym_cell_strs_ints_hash(Zall{i}{1},Zall{i}{2});
        end
    else
        Hash_code(i) = str_hash(Zall{i});
    end
end
hash_code = int_hash(Hash_code);

end

%% AVOIDING FUNCTION CALLS (this is the upper version without subfunction calls)
% function hash_code = net_hash(Zall)
% 
%     hash_max = 100003; % is the same prime as in sym_double_hash
% 
% mul_256_ttpo_1_to_10 = [256 65536 76715 38452 43418 14675 56689 11949 58854 66174];
% 
% nZall1 = length(Zall);
% Hash_code = zeros(1,nZall1);
% for i = 1:nZall1
%     if isa(Zall{i},'cell')
%         if size(Zall{i},1) == 1
%             %% Hash_code(i) = sym_strs_hash(Zall{i}{:});
%             
%             d = zeros(1,nargin);
%             for j = 1:length(Zall{i})
%                 %% d(j) = str_hash(Zall{i}{j});
%                 
%                 b = double(Zall{i}{j});
%                 
%                 % b = powermod(b,23,hash_max); % this should not be neccessary
%                 
%                 leb = length(b);
%                 if leb <= 10
%                     d(j) = mod(mul_256_ttpo_1_to_10(1:leb)*b(:),hash_max);
%                 else
%                     
%                     for k = 2:length(b)
%                         b(k:end) = mod(256*b(k:end),hash_max);
%                     end
%                     
%                     d(j) = mod(sum(b),hash_max);
%                 end
%             end
%             
%             %% hash_code = sym_ints_hash(d);
%             pr = 1;
%             for j = 1:length(d)
%                 pr = mod(pr*d(j),hash_max);
%             end
%             
%             Hash_code(i) = mod(pr+d(:)'*d(:),hash_max);
%             
%         else
%             %% Hash_code(i) = sym_cell_strs_ints_hash(Zall{i}{1},Zall{i}{2});
%             
%             Z1 = Zall{i}{1};
%             Z2 = Zall{i}{2};
%             leb = size(Z1,2);
%             d = zeros(1,leb);
%             for j = 1:leb
%                 %% b(j) = str_int_hash(Z1{j},Z2(j));
%                 b = double(Z1{j});
%                 
%                 % b = powermod(b,23,hash_max);
%                 
%                 leb = length(b);
%                 if leb <= 10
%                     hash_code = mod(mul_256_ttpo_1_to_10(1:leb)*b(:),hash_max);
%                 else
%                     
%                     for k = 2:length(b)
%                         b(k:end) = mod(256*b(k:end),hash_max);
%                     end
%                     
%                     hash_code = mod(sum(b),hash_max);
%                 end
%                 
%                 d(j) = mod(hash_code*(Z2(j)+101),hash_max);
%             end
%             
%             %% hash_code = sym_ints_hash(b);
%             pr = 1;
%             for j = 1:length(d)
%                 pr = mod(pr*d(j),hash_max);
%             end
%             
%             Hash_code(i) = mod(pr+d(:)'*d(:),hash_max);
%         end
%     else
%         %% Hash_code(i) = str_hash(Zall{i});
%         b = double(Zall{i});
%         
%         % b = powermod(b,23,hash_max); % this should not be neccessary
%         
%         leb = length(b);
%         if leb <= 10
%             Hash_code(i) = mod(mul_256_ttpo_1_to_10(1:leb)*b(:),hash_max);
%         else
%             
%             for j = 2:length(b)
%                 b(j:end) = mod(256*b(j:end),hash_max);
%             end
%             
%             Hash_code(i) = mod(sum(b),hash_max);
%         end
%     end
% end
% %% hash_code = int_hash(Hash_code);
% leb = length(Hash_code);
% if leb <= 10
%     hash_code = mod(mul_256_ttpo_1_to_10(1:leb)*Hash_code(:),hash_max);
% else
%     
%     for i = 2:length(Hash_code)
%         Hash_code(i:end) = mod(256*Hash_code(i:end),hash_max);
%     end
%     
%     hash_code = mod(sum(Hash_code),hash_max);
% end
% 
% end