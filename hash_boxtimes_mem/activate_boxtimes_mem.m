function activate_boxtimes_mem(hash_list_length)
% ACTIVATE_BOXTIMES_MEM  activates the memory hash for boxtimes
%
%   ACTIVATE_BOXTIMES_MEM(hash_list_length) will use a hash list of
%   according size. Without input, the default value is 2500.
%
%   If already activated, past results are retained.
%
%   See also: TENSOR_NODE_NOTATION6_NESTED_BOXTIMES.mlx

global BOXTIMES_MEM

if exist('BOXTIMES_MEM','var') && isa(BOXTIMES_MEM,'struct') && isfield(BOXTIMES_MEM,'hash_fill_nr') ...
        && ~(BOXTIMES_MEM.hash_fill_nr==0) && isfield(BOXTIMES_MEM,'HASHLIST') && isfield(BOXTIMES_MEM,'mem')
    
    BOXTIMES_MEM.active = true;
     
    if nargin ~= 0      
        current_length = length(BOXTIMES_MEM.mem);
        
        if current_length < hash_list_length
            BOXTIMES_MEM.mem = [BOXTIMES_MEM.mem, cell(1,hash_list_length-current_length)];
            BOXTIMES_MEM.HASHLIST = [BOXTIMES_MEM.HASHLIST, -ones(1,hash_list_length-current_length)];
        else
            selector = mod((BOXTIMES_MEM.hash_fill_nr-hash_list_length+1:BOXTIMES_MEM.hash_fill_nr)-1,current_length)+1;
            BOXTIMES_MEM.mem = BOXTIMES_MEM.mem(selector);
            BOXTIMES_MEM.HASHLIST = BOXTIMES_MEM.HASHLIST(selector);
            
        end
    end
else
    if nargin == 0
        hash_list_length = 2500;
    end
    
    BOXTIMES_MEM = struct;
    BOXTIMES_MEM.active = true;
    BOXTIMES_MEM.HASHLIST = -ones(1,hash_list_length);
    BOXTIMES_MEM.mem = cell(1,hash_list_length);
    BOXTIMES_MEM.hash_fill_nr = 0;
end