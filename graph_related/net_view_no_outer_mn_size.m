function net_view_no_outer_mn_size(n_red,varargin)

if nargin-1 >= 3 && isa(varargin{end-1},'char') && strcmp(varargin{end-1},'Layout')
    Layout = varargin{end};
    varargin = varargin(1:end-2);
else
    Layout = [];
end
    

[Z,glob_repl,outer_mn_id] = get_mn_replacements(varargin);
[Z,n,true_mode_name_map] = rename_mn(Z,glob_repl);

nN = length(Z);
m = cell(1,nN);
for s = 1:nN
    m{s} = Z{s}.mode_names;
end

% ghost_mode_names = {};
% % ghost nodes:
V_ghost = false(1,length(Z));
for i = 1:length(Z)
    if isfield(Z{i},'ghost')
        V_ghost(i) = 1;
        %         ghost_mode_names = [m{i},ghost_mode_names];
    end
end
V_ghost = find(V_ghost);

% Z(V_ghost) = [];
% nN = length(Z);

% V_ghost = [];

% num_nodes = length(Z);
edgemap = corresponding_edgemap(Z);
modes = fieldnames(edgemap);
outer_mode_names = mna('mu',outer_mn_id);
for i = 1:length(modes)
    n.(modes{i}) = n_red.(true_mode_name_map.(modes{i}));
end
inner_mode_names = str_setdiff(modes,outer_mode_names);
%
%
% outer_ghost = str_intersect(inner_mode_names,ghost_mode_names);
% inner_mode_names = str_setdiff(inner_mode_names,outer_ghost);
%
% outer_mode_names = str_setdiff(outer_mode_names,ghost_mode_names);
% outer_mode_names = str_union(outer_ghost,outer_mode_names);

% outer_mode_names = str_setdiff(outer_mode_names,inner_ghost);
%
% outer_mode_names = str_setdiff(outer_mode_names,ghost_mode_names);
%
%
% inner_mode_names = str_setdiff(inner_mode_names,ghost_mode_names);
outer_edges = [];




G = graph;
G = addnode(G,nN);
extra_node_nr = nN;
crossing_nodes = [];
outer_edgemap_edges = zeros(length(outer_mode_names),nN);
inner_edgemap_edges = zeros(length(inner_mode_names),nN);
outer_edgemap_dict = zeros(length(outer_mode_names),1);
inner_edgemap_dict = zeros(length(outer_mode_names),1);

% outer_mode_names
for i = 1:length(outer_mode_names)
    edge = edgemap.(outer_mode_names{i});
    outer_edgemap_edges(i,edge) = 1;
    le = length(edge);
    
    [~,ind] = ismember(outer_edgemap_edges(i,:),outer_edgemap_edges(1:i-1,:),'rows');
%     ind = find(ind,1);
    if ~ind %isempty(ind)
        
        if le == 1
            extra_node_nr = extra_node_nr + 1;
            m = [m,{outer_mode_names(i)}];
            outer_edgemap_dict(i) = extra_node_nr;
            
            G = addedge(G,edge(1),extra_node_nr);
            outer_edges = [outer_edges; edge(1),extra_node_nr];
        else
            extra_node_nr = extra_node_nr + 1;
            crossing_nodes = [crossing_nodes,extra_node_nr];
            m = [m,{outer_mode_names(i)}];
            outer_edgemap_dict(i) = extra_node_nr;
            
            for j = 1:le
                G = addedge(G,edge(j),extra_node_nr);
            end
            extra_node_nr = extra_node_nr + 1;
            m = [m,{outer_mode_names(i)}];
            
            G = addedge(G,extra_node_nr-1,extra_node_nr);
            outer_edges = [outer_edges; extra_node_nr-1,extra_node_nr];
        end
    else
        if le == 1
            m{outer_edgemap_dict(ind)} = [m{outer_edgemap_dict(ind)},outer_mode_names{i}];
        else
            m{outer_edgemap_dict(ind)} = [m{outer_edgemap_dict(ind)},outer_mode_names{i}];
            m{outer_edgemap_dict(ind)+1} = [m{outer_edgemap_dict(ind)+1},outer_mode_names{i}];
        end
    end
    
end

% inner_mode_names
for i = 1:length(inner_mode_names)
    edge = edgemap.(inner_mode_names{i});
    le = length(edge);
    inner_edgemap_edges(i,edge) = 1;
    
    [~,ind] = ismember(inner_edgemap_edges(i,:),inner_edgemap_edges(1:i-1,:),'rows');
%     ind = find(ind,1);
      
    if ~ind % isempty(ind)
        
        if le == 2
            if max(edge)>numnodes(G) || ~findedge(G,edge(1),edge(2))
                G = addedge(G,edge(1),edge(2));
            end
        else
            extra_node_nr = extra_node_nr + 1;
            crossing_nodes = [crossing_nodes,extra_node_nr];
            m = [m,{inner_mode_names(i)}];
            inner_edgemap_dict(i) = extra_node_nr;
            
            for j = 1:le
                G = addedge(G,edge(j),extra_node_nr);
            end
        end
        
    else
        if le ~= 2
            m{inner_edgemap_dict(ind)} = [m{inner_edgemap_dict(ind)},inner_mode_names{i}];
        end
    end
end

network_plot(G,m,n,outer_edges,true_mode_name_map,crossing_nodes,V_ghost,Layout)

% edge_label = cell(1,numedges(G));
% m = [m,m_add];
%
% if nargin == 3
%     weight = ones(numedges(G),1);
% end
%
%
%
%
% edge_label = cell(1,numedges(G));
% m = [m,m_add];
%
% if nargin == 3
%     weight = ones(numedges(G),1);
% end



% if ~isempty(outer_mn_id)
%     % add ghost node with out mode names
%     for i = 1:length(outer_mn_id)
%         num_nodes = num_nodes + 1;
%         mn = mna('mu',outer_mn_id(i));
%         edgemap.(mn{1}) = [edgemap.(mn{1}),num_nodes];
%     end
% end
%
% % % ghost nodes:
% V_ghost = false(1,length(Z));
% for i = 1:length(Z)
%     if isfield(Z{i},'ghost')
%         V_ghost(i) = 1;
%     end
% end
%
% Z(V_ghost) = [];
%
% nN = length(Z);
% m = cell(1,nN);
% for s = 1:nN
%     m{s} = Z{s}.mode_names;
% end
%
% for i = 1:length(outer_mn_id)
%     m = [m,{[mna('mu',outer_mn_id(i)),'virt']}];
% end
%
% [G,m] = derive_G(m);
%
% %
% % % turn artificial node into ghost node
% % V_ghost(end-length(outer_mn_id)+1:end) = true;
% % V_ghost = find(V_ghost);
%
% outer_modes = mna('mu',outer_mn_id); %,'virt'];
%
% [G,m] = net_derive_G(Z);
% graph_view(G,m,n,true_mode_name_map,outer_modes);

end