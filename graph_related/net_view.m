function net_view(varargin)
% NET_VIEW  plots the network defined through the given nodes
%
%   NET_VIEW(...) plots the network corresponding to the contractions
%   which the function boxtimes(...) would proceed. 
%
%   Edges depict contractions, while dashed lines are modes which will
%   remain after the contraction. The linewidth correspond to ranks/mode
%   sizes for edges/legs.
%
%   NET_VIEW(...,'Layout',Layout) additionally defines a layout in which
%   the network is plotted. For possible Layout strings, see the standard
%   graph plot in Matlab.
%
%   See also: See also: TENSOR_NODE_NOTATION2_BOXTIMES.mlx, BOXTIMES,
%   NETWORK_PLOT


% check if layour defined
if nargin >= 3 && isa(varargin{end-1},'char') && strcmp(varargin{end-1},'Layout')
    Layout = varargin{end};
    varargin = varargin(1:end-2);
else
    Layout = [];
end
    
% apply unique mode name marker mu_1,mu_2,...
[Z,glob_repl,outer_mn_id] = get_mn_replacements(varargin);
[Z,n,true_mode_name_map] = rename_mn(Z,glob_repl);

% save mode names in m
nN = length(Z);
m = cell(1,nN);
for s = 1:nN
    m{s} = Z{s}.mode_names;
end

% mark ghost nodes:
V_ghost = false(1,length(Z));
for i = 1:length(Z)
    if isfield(Z{i},'ghost')
        V_ghost(i) = 1;
    end
end
V_ghost = find(V_ghost);

% calculate edgemap (field pointing to connected nodes)
edgemap = corresponding_edgemap(Z);
modes = fieldnames(edgemap);

% summarize outer modes names as found in outer_mn_id
outer_mode_names = mna('mu',outer_mn_id);
% summarize inner_mode_names (the rest)
inner_mode_names = str_setdiff(modes,outer_mode_names);

outer_edges = [];

%% transfer to Matlab graph (using artifical nodes to create a hypergraph) 
% and collect additional information
G = graph;
G = addnode(G,nN);

extra_node_nr = nN;
crossing_nodes = []; % artifical nodes for hypergraph

outer_edgemap_edges = zeros(length(outer_mode_names),nN);
inner_edgemap_edges = zeros(length(inner_mode_names),nN);
outer_edgemap_dict = zeros(length(outer_mode_names),1);
inner_edgemap_dict = zeros(length(outer_mode_names),1);

% outer_mode_names
for i = 1:length(outer_mode_names)
    edge = edgemap.(outer_mode_names{i});
    outer_edgemap_edges(i,edge) = 1;
    le = length(edge);
    
    [~,ind] = ismember(outer_edgemap_edges(i,:),outer_edgemap_edges(1:i-1,:),'rows');

    if ~ind 
        
        if le == 1
            extra_node_nr = extra_node_nr + 1;
            m = [m,{outer_mode_names(i)}];
            outer_edgemap_dict(i) = extra_node_nr;
            
            G = addedge(G,edge(1),extra_node_nr);
            outer_edges = [outer_edges; edge(1),extra_node_nr];
        else
            extra_node_nr = extra_node_nr + 1;
            crossing_nodes = [crossing_nodes,extra_node_nr];
            m = [m,{outer_mode_names(i)}];
            outer_edgemap_dict(i) = extra_node_nr;
            
            for j = 1:le
                G = addedge(G,edge(j),extra_node_nr);
            end
            extra_node_nr = extra_node_nr + 1;
            m = [m,{outer_mode_names(i)}];
            
            G = addedge(G,extra_node_nr-1,extra_node_nr);
            outer_edges = [outer_edges; extra_node_nr-1,extra_node_nr];
        end
    else
        if le == 1
            m{outer_edgemap_dict(ind)} = [m{outer_edgemap_dict(ind)},outer_mode_names{i}];
        else
            m{outer_edgemap_dict(ind)} = [m{outer_edgemap_dict(ind)},outer_mode_names{i}];
            m{outer_edgemap_dict(ind)+1} = [m{outer_edgemap_dict(ind)+1},outer_mode_names{i}];
        end
    end 
end

% inner_mode_names
for i = 1:length(inner_mode_names)
    edge = edgemap.(inner_mode_names{i});
    le = length(edge);
    inner_edgemap_edges(i,edge) = 1;
    
    [~,ind] = ismember(inner_edgemap_edges(i,:),inner_edgemap_edges(1:i-1,:),'rows');
      
    if ~ind 
        
        if le == 2
            if max(edge)>numnodes(G) || ~findedge(G,edge(1),edge(2))
                G = addedge(G,edge(1),edge(2));
            end
        else
            extra_node_nr = extra_node_nr + 1;
            crossing_nodes = [crossing_nodes,extra_node_nr];
            m = [m,{inner_mode_names(i)}];
            inner_edgemap_dict(i) = extra_node_nr;
            
            for j = 1:le
                G = addedge(G,edge(j),extra_node_nr);
            end
        end
        
    else
        if le ~= 2
            m{inner_edgemap_dict(ind)} = [m{inner_edgemap_dict(ind)},inner_mode_names{i}];
        end
    end
end

% pass the raw data to network_plot
network_plot(G,m,n,outer_edges,true_mode_name_map,crossing_nodes,V_ghost,Layout)

end