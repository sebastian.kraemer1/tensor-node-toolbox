function G = derive_G(m)
% DERIVE_G  derives the graph corresponding to mode names in m
%
%   See also: CORRESPONDING_EDGEMAP

% same functionality as corresponding_edgemap.m
edgemap = cell(0);
for i = 1:length(m)
    for j = 1:length(m{i})
        if ~isfield(edgemap,m{i}{j})
            edgemap.(m{i}{j}) = i;
        else
            edgemap.(m{i}{j}) = [edgemap.(m{i}{j}); i];
        end
    end
end

modes = fieldnames(edgemap);
G = graph;

for i = 1:length(modes)
    edgemap.(modes{i}) = unique(edgemap.(modes{i}));
    edge = edgemap.(modes{i});
    if length(edge) == 2 && edge(1) ~= edge(2)
        if max(edge)>numnodes(G) || ~findedge(G,edge(1),edge(2))
            G = addedge(G,edge(1),edge(2));
        end
    elseif length(edge) > 2
        error('Given mode names do not correspond to a plain network');
    end 
end

end

