function graph_view(G,m,n,true_mode_name_map)
% GRAPH_VIEW  plots the graph G together with mode names m
%
%   Each edge is labeled with the common mode names of the two nodes it
%   connects.
%
%   The outer mode names are determined automatically and plotted as dotted
%   lines additional to the graph.
%
%   Examples:
%       alpha = mna('alpha',1:8);
%   	S = binary_tree(alpha);
%       [G,m,M,r] = RTLGRAPH(alpha,S);
%
%       graph_view(G,m);

if nargin < 3
    n = [];  
end
if nargin < 4
     true_mode_name_map = [];
end

k = numnodes(G);
m = m(1:k);
mouter = m;
m_add = {};
outer_edges = [];  
    
for i = 1:k
    Ne = neighbors(G,i);
    for j = 1:length(Ne)
        mouter{i} = setdiff(mouter{i},m{Ne(j)});
    end
    if ~isempty(mouter{i})
        outer_edges = [outer_edges; i,numnodes(G)+1];
        G = addedge(G,i,numnodes(G)+1); % add virtual edge
        m_add = [m_add,{mouter{i}}];
    end
end
m = [m,m_add];

crossing_nodes = [];

network_plot(G,m,n,outer_edges,true_mode_name_map,crossing_nodes,[],[])