function [M,m_prec] = derive_M(G,m,r)
% DERIVE_M  Determine cell M for which M{k} = intersect(branch_r(k),
% <outer_mode_names>).
%
%   G must be a tree graph, m be a cell containing the mode names for each
%   node and r be the root.
%
%   Examples:
%       alpha = mna('alpha',1:8);
%       S = binary_tree(alpha);
%       [G,m,M,r] = RTLGRAPH(alpha,S);
%       M_copy = derive_M(G,m,r);
%
%   See also: RTLGRAPH, BINARY_TREE, LINEAR_TREE, MNA

M = cell(numnodes(G),1);
m_prec = cell(numnodes(G),1);
mouter = m;

derive_M_rec(r,[]);

    function derive_M_rec(r,p)
        M{r} = {};
        
              
        for h = int_setdiff(neighbors(G,r)',p)
            % remove common mode names (that is all but outer ones)
            common = str_intersect(m{r},m{h});
            mouter{r} = str_setdiff(mouter{r},common);
            mouter{h} = str_setdiff(mouter{h},common);
            
            % accumulate outer mode names from its descendants
            derive_M_rec(h,r);
            M{r} = str_union(M{r},M{h});
            m_prec{h} = common{1};
        end
        
        M{r} = str_union(M{r},mouter{r});
    end

end