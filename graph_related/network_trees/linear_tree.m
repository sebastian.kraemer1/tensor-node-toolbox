function S = linear_tree(t)
% LINEAR_TREE  To be used by RTLGRAPH for a TT tree
%
%   S = LINEAR_TREE(t) is a cell containing a sequence of subsets (cells) 
%   of the mode names in the cell t. The sequence defines successive
%   splitting of modes to obtain a linear tree, that is the TT format.
%   
%   Examples:
%       outer_mode_names = mna('alpha',[1,2,3,4]);
%       S = linear_tree(outer_mode_names); 
%       S{1} % returns {'alpha_2','alpha_3','alpha_4'}
%       S{2} % returns {'alpha_3','alpha_4'}
%       S{3} % returns {'alpha_4'}
%
%   See also RTLGRAPH and MNA

d = length(t);
S = cell(1,d-1);
for i = 1:d-1
    S{i} = t(:,i+1:d);
    S{i} = S{i}(:)';
end
end