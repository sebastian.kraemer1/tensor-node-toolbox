function S = random_tree(t,limit)
% RANDOM_TREE  To be used by RTLGRAPH for a pseudo random tree
%
%   S = RANDOM_TREE(t,limit) is a cell containing a sequence of subsets 
%   (cells) of the mode names in the cell t. The sequence defines 
%   successive splitting of modes to obtain a pseudo random tree. 
%   limit is a limit to the number of edges of any one node in the result 
%   graph.
%
%   The likelihood for each possible sequence is however not equal, and
%   some may not even be possible to generate.
%   
%   Examples:
%       rng(1)
%       outer_mode_names = mna('alpha',[1,2,3,4]);
%       S = random_tree(outer_mode_names); 
%       S{1} % returns {'alpha_1','alpha_2','alpha_3'}
%       S{2} % returns {'alpha_1'}
%       S{3} % returns {'alpha_2','alpha_3'}
%       S{4} % returns {'alpha_2'}
%
%   See also RTLGRAPH, MNA, BINARY_TREE, LINEAR_TREE

limit = max(limit,3);
d = size(t,2);
S = cell(1,2*d-2);
B = {t};
numConB = 1;
num_remain = d-1;
i_S = 0;

while num_remain > 0
    i_S = i_S + 1;
    
    i = randi([1 size(B,2)]);
    
    if numConB(i) >= limit
        j = size(B{i},2);
    else
        j = randi([1 size(B{i},2)]);
        if numConB(i) <= 2
            j = min(j,size(B{i},2)-1);
        end
    end
    
    b = B{i}(:,1:j);
    S{i_S} = b;
    
    B{i} = B{i}(:,j+1:end); % = setdiff(B{i},b);
    if ~isempty(B{i})
        num_remain = num_remain - 1;
    end
    if i_S == 1 && size(B{i},2) == 1 || size(B{i},2) == 0
        B = B(:,[1:i-1,i+1:end]);
        numConB = numConB([1:i-1,i+1:end]);
    else
        numConB(i) = numConB(i) + 1;
    end
      
    if size(b,2) > 1
        B = [B,{b}];
        numConB = [numConB,2];
    end
end

S = S(1:i_S);

for i = 1:length(S)
    S{i} = S{i}(:)';
end

end