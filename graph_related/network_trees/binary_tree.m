function S = binary_tree(t)
% BINARY_TREE  To be used by RTLGRAPH for a binary HT tree
%
%   S = BINARY_TREE(t) is a cell containing a sequence of subsets (cells) 
%   of the mode names in the cell t. The sequence defines successive
%   splitting of modes to obtain a binary HT tree without root transfer
%   tensor.
%   
%   Examples:
%       outer_mode_names = mna('alpha',[1,2,3,4]);
%       S = binary_tree(outer_mode_names); 
%       S{1} % returns {'alpha_1','alpha_2'}
%       S{2} % returns {'alpha_1'}
%       S{3} % returns {'alpha_2'}
%       S{4} % returns {'alpha_3'}
%       S{5} % returns {'alpha_4'}
%
%   See also:  TENSOR_NODE_NOTATION7_PLAIN_NETWORKS.mlx, RTLGRAPH, MNA


j = floor(size(t,2)/2);
S = binary_rec({t(:,1:j)},t(:,1:j));
S = binary_rec(S,t(:,j+1:end));

for i = 1:length(S)
    S{i} = S{i}(:)';
end
end

function S = binary_rec(S,t)
if size(t,2) > 1
    j = ceil(size(t,2)/2);
    S = [S,{t(:,1:j)},{t(:,j+1:end)},binary_rec([],t(:,1:j)),binary_rec([],t(:,j+1:end))];
end
end