function S = tucker_tree(t)

d = length(t);
S = cell(1,d);
for i = 1:d
    S{i} = t(:,i);
    S{i} = S{i}(:)';
end
end