function [G,m] = net_derive_G(Net)
% NET_DERIVE_G  first save mode names in order to derive the graph of Net
%
%   See also: LINSOLVE_EQUALNET

if ~isa(Net,'cell')
    Net = {Net};
end

nN = length(Net);
m = cell(1,nN);

for s = 1:nN
    m{s} = Net{s}.mode_names;
end

G = derive_G(m);

end