function Br = branches(G,r)
% BRANCHES  calculates the branches in a tree from a root r
%
%   Br = BRANCHES(G,r) expects a tree G and root node r. It returns a cell
%   Br for which Br{k}{2} is the branch from node k with respect to the root
%   r, while Br{k}{1} is the complement.
%
%   Br{r}{2} always contains all nodes, while Br{r}{1} is empty.
%
%   See also: TENSOR_NODE_NOTATION8_STANDARD_REPRESENTATIONS.mlx

k = numnodes(G);
Br = cell(1,k);

branch_rec_rtl(r,[])
Br{r}{1} = [];
branch_rec_ltr(r,[])

    function branch_rec_rtl(b,P)
        Neighb = neighbors(G,b)';
        for h = int_setdiff(Neighb,P)
            branch_rec_rtl(h,b);
        end
        
        Br{b} = cell(1,2);
            
        Br{b}{2} = b;
        for h = int_setdiff(Neighb,P)
            Br{b}{2} = [Br{b}{2},Br{h}{2}];
        end
        
        Br{b}{2} = sort(Br{b}{2},'ascend');
        
    end

    function branch_rec_ltr(b,P)
        
        for h = int_setdiff(neighbors(G,b)',P)
            Br{h}{1} = [b,Br{b}{1}];
            for u = int_setdiff(neighbors(G,b)',[P,h])
                Br{h}{1} = [Br{h}{1},Br{u}{2}];
            end
            branch_rec_ltr(h,b);
        end
        Br{b}{1} = sort(Br{b}{1},'ascend');
    end

end