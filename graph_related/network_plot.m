function network_plot(G,m,n,outer_edges,true_mode_name_map,crossing_nodes,V_ghost,Layout)
% NETWORK_PLOT  plots the graph information mostly provided by net_view.m    
%
% See also: NET_VIEW

edge_label = cell(1,numedges(G));

if ~isempty(n)
    weight = ones(numedges(G),1);
end

painted = {};
for i = numedges(G):-1:1
    common = str_intersect(m{G.Edges.EndNodes(i,1)},m{G.Edges.EndNodes(i,2)});
  
    if ~isempty(n) 
        for j = 1:length(common)
            weight(i) = weight(i)*n.(common{j});
        end
    end
    
    common = str_setdiff(common,painted);
    painted = [painted,common];
    
    if ~isempty(true_mode_name_map)
        for j = 1:length(common)
            common{j} = true_mode_name_map.(common{j});
        end
    end
    
    for j = 1:length(common)-1
        edge_label{i} = [edge_label{i},common{j},', '];
    end
    if ~isempty(common)
        edge_label{i} = [edge_label{i},common{end}];
    end
       
    edge_label{i} = texlabel(edge_label{i});
end

if isempty(Layout)
    if numnodes(G) > 40
        Layout = 'force3'; % layered
    else
        Layout = 'force';
    end
end
h = plot(G,'Layout',Layout,'EdgeLabel',edge_label,'MarkerSize',6);

if ~isempty(outer_edges)
    highlight(h,outer_edges(:,1),outer_edges(:,2),'LineStyle',':')
end
if ~isempty(outer_edges)
    labelnode(h,outer_edges(:,2),'');
    highlight(h,outer_edges(:,2),'Marker','none')
end
labelnode(h,crossing_nodes,'');
highlight(h,crossing_nodes,'Marker','none')
highlight(h,V_ghost,'NodeColor','r')

h.EdgeLabel = '';
xd = get(h, 'XData');
yd = get(h, 'YData');
Xd = (xd(G.Edges.EndNodes(:,1)) + xd(G.Edges.EndNodes(:,2)))/2;
Yd = (yd(G.Edges.EndNodes(:,1)) + yd(G.Edges.EndNodes(:,2)))/2;
if strcmp(Layout,'force3')
zd = get(h, 'ZData');
Zd = (zd(G.Edges.EndNodes(:,1)) + zd(G.Edges.EndNodes(:,2)))/2;
text(Xd, Yd, Zd, edge_label, 'FontSize',15, 'HorizontalAlignment','left', 'VerticalAlignment','middle')
else
    text(Xd, Yd, edge_label, 'FontSize',15, 'HorizontalAlignment','left', 'VerticalAlignment','middle')
end

if ~isempty(n)
    h.LineWidth = min(10,max(max(weight),4))*weight/max(weight);
end
end