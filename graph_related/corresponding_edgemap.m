function edgemap = corresponding_edgemap(Net) 
% CORRESPONDING_EDGEMAP  Returns a struct mapping edges to its mode names.
%
%   edgemap = CORRESPONDING_EDGEMAP(N) takes a tensor network Net (as a 
%   cell of nodes) and returns a struct edgemap such that each
%   edgemap.mn is a cell containing all indices i for which 'mn' is a mode 
%   name of Net{i}.
%
%   Example:
%       N1 = init_node({'alpha','beta','gamma'},[1,2,3]);
%       N2 = init_node({'beta','gamma','delta'},[2,3,4]);
%       Net = {N1,N2};
%       edgemap = corresponding_edgemap(Net) 
%
%       % returns:
%       %   edgemap = 
% 
%       %     struct with fields:
% 
%       %       alpha: 1
%       %        beta: [2×1 double]
%       %       gamma: [2×1 double]
%       %       delta: 2    
%
%   See also: INIT_NODE

edgemap = cell(0);
for i = 1:length(Net)
    for j = 1:length(Net{i}.mode_names)
        if ~isfield(edgemap,Net{i}.mode_names{j})
            edgemap.(Net{i}.mode_names{j}) = i;
        else
            edgemap.(Net{i}.mode_names{j}) = [edgemap.(Net{i}.mode_names{j}); i];
        end
    end
end
end