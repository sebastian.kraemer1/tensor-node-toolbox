function [a,Ja,Jb] = listdiff(a,b,varargin)
% example: listdiff({'a','b','a','c'},{'a','b'}) = {'a','c'}
% requires b to be contained in a
% 
% if ~isa(b,'cell') && ~isempty(b)
%     b = {b};
% end
% 
% J = zeros(1,length(b));
% for i = 1:length(b)
%     [~,j] = ismember(b{i},a);
%     J(i) = j;
%     a{j} = '';
% end
% % a = a(setdiff(1:length(a),J));
% a(J) = [];
% end

%% no longer requires b to be contained in a
if ~isa(b,'cell') && ~isempty(b)
    b = {b};
end

Ja = false(1,length(a));
Jb = false(1,length(b));
for i = 1:length(b)
    [Jb(i),j] = ismember(b{i},a);

    if Jb(i)
        Ja(j) = true;
        a{j} = '';
    end
end

a(Ja) = [];
end

