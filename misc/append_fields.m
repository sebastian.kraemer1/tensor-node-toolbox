function n1 = append_fields(varargin)
% APPEND_FIELDS  merges and appends the fields of multiple structs
%
%   APPEND_FIELDS(f1,f2,...) merges the fields of f1,f2,...
%
%   If a field is contained in both f1 and f2, these entries are
%   concecrated (the function expects this to be possible).
%
%   See also: TENSOR_NODE_NOTATION2_BOXTIMES.mlx

if nargin == 2
    n1 = varargin{1};
    n2 = varargin{2};
    
    names2 = fieldnames(n2);
    for i = 1:numel(names2)
        n1 = append_field_entry(n1,names2{i},n2.(names2{i}));
%         n1.(names2{i}) = n2.(names2{i}); % earlier version did not append
    end
else
    n2 = append_fields(varargin{2:end});
    n1 = append_fields(varargin{1},n2);
end


end

