function C = getfields(S,fields)
% GETFIELDS  returns a cell of entries related to fields
%
%   See also: TENSOR_NODE_NOTATION8_STANDARD_REPRESENTATIONS.mlx

C = cell(size(fields));

if numel(C) == 0
    C = [];
end    

for i = 1:numel(C)
    C{i} = S.(fields{i});
end
