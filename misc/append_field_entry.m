function structname = append_field_entry(structname,fieldname,value)
    
if isfield(structname,fieldname)
    structname.(fieldname) = [structname.(fieldname),value];
else
    structname.(fieldname) = value;
end

