function n1 = merge_fields(varargin)
% MERGE_FIELDS  merges the fields of multiple structs
%
%   MERGE_FIELDS(f1,f2,...) merges the fields of f1,f2,...
%
%   See also: TENSOR_NODE_NOTATION2_BOXTIMES.mlx

if nargin == 2
    n1 = varargin{1};
    n2 = varargin{2};
    
    names2 = fieldnames(n2);
    for i = 1:numel(names2)
%         n1 = append_field_entry(n1,names2{i},n2.(names2{i}));
        n1.(names2{i}) = n2.(names2{i}); % is not supposed to append
    end
else
    n2 = merge_fields(varargin{2:end});
    n1 = merge_fields(varargin{1},n2);
end


end

