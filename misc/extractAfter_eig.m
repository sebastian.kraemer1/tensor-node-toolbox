function strAfter = extractAfter_eig(str,pattern)

strAfter = cell(size(str));
J = strfind(str,pattern);

for i = 1:numel(str)
    strAfter{i} = str{i}(J{i}+length(pattern):end);
end