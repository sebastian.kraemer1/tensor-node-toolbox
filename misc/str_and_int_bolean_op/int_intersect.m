function [acapb,awob,bwoa] = int_intersect(a,b)
% INT_INTERSECT  Equivalent to intersect(a,b), setdiff(a,b)
% and setdiff(b,a) for integer vectors a,b if each has unique entries.
%
%   [acapb,awob,bwoa] = INT_INTERSECT(a,b) returns
%   acapb = intersect(a,b)
%   awob = setdiff(a,b) % if asked for
%   bwoa = setdiff(b,a) % if asked for
%   but without overhead (and relying on correct input).
%
%   See also: INTERSECT, SETDIFF, STR_INTERSECT

mab = max([1,max(a),max(b)]);
sp = sparse(1,a(:),1,1,mab) + sparse(1,b(:),2,1,mab);
acapb = find(sp==3); % = intersect(a,b)
if nargout > 1
    awob = find(sp==1); % = setdiff(a,b)
end
if nargout > 2
    bwoa = find(sp==2); % = setdiff(b,a)
end

end