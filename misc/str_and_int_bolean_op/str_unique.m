function aun = str_unique(a)

    IA = false(size(a));
    for j = 1:numel(a)
        cmp = find(strcmp(a{j},a),1);
        IA(cmp) = 1;
    end
    aun = a(IA);
