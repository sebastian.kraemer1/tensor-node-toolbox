function [acapb,awob,bwoa] = ustr_intersect(a,b)
% STR_INTERSECT  Equivalent to intersect(a,b), setdiff(a,b)
% and setdiff(b,a) for string cells a,b, each having distinct entries!
%
%   [acapb,awob,bwoa] = STR_INTERSECT(a,b) returns
%   acapb = intersect(a,b)
%   awob = setdiff(a,b) % if asked for
%   bwoa = setdiff(b,a) % if asked for
%   but without overhead (and relying on correct input).
%
%   [acapb,awob,bwoa,IA,IB] = STR_INTERSECT(a,b) also returns
%   the logical indices IA, IB for which acapb = a(IA) = b(IB).
%
%   See also: INTERSECT, SETDIFF, INT_INTERSECT

    IA = false(size(a));
    IB = false(size(b));
    for j = 1:numel(a)
        cmp = find(strcmp(a{j},b),1);
        IA(j) = ~isempty(cmp);
        IB(cmp) = 1; % no uniqueness
    end
    acapb = b(IB);
    if nargout > 1
        awob = a(~IA);
    end
    if nargout > 2
        bwoa = b(~IB);
    end
end

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


% function [AI, BI] = string_intersect_setdiff(A, B)
% nA = numel(A);
% nB = numel(B);
% if nA <= nB
%    % Collect the index of the first occurrence in B for every A:
%    M = zeros(1, nA);
%    for iA = 1:nA
%       Ind = find(strcmp(A{iA}, B), 1);
%       if isempty(Ind) == 0
%          M(iA) = Ind(1);
%       end
%    end
% 
% else  % nB <= nA, B is smaller, so it is cheaper to loop over B:
%    % Mark every A which equal the current B
%    M = zeros(1, nA);
%    for iB = nB:-1:1
%       M(strcmp(B{iB}, A)) = iB;  % Same as M(find(strcmp()))
%    end
% end
% 
% AI = find(M);  % If any occurrence was found, this A exists...
% BI = M(AI);    % at this index in B
% end
