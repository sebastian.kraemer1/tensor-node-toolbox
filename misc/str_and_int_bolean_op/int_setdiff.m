function awob = int_setdiff(a,b)
% INT_SETDIFF  Equivalent to setdiff(a,b) for integer vectors a,b if each
% has unique entries.
%
%   acapb = INT_SETDIFF(a,b) returns
%   awob = setdiff(a,b) 
%   but without overhead (and relying on correct input).
%
%   See also: INTERSECT, SETDIFF, STR_INTERSECT

miab = min([min(a),min(b)])-1;
if isempty(miab)
    miab = 0;
end
mab = max([1,max(a)-miab,max(b)-miab]);

sp = sparse(1,a(:)-miab,1,1,mab) + sparse(1,b(:)-miab,2,1,mab);
awob = find(sp==1)+miab; % = setdiff(a,b)

end