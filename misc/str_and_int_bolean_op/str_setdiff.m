function [awob,IA] = str_setdiff(a,b)
% STR_SETDIFF  Equivalent to setdiff(a,b) for string cells a,b
%
%   awob = STR_SETDIFF(a,b) returns
%   awob = setdiff(a,b)
%   but without overhead (and relying on correct input).
%
%   See also: INTERSECT, SETDIFF, INT_INTERSECT

    IA = false(size(a));
    for j = 1:numel(b)
        cmp = strcmp(b{j},a);
        IA = IA | cmp;
    end

    awob = a(~IA);
end

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


% function [AI, BI] = string_intersect_setdiff(A, B)
% nA = numel(A);
% nB = numel(B);
% if nA <= nB
%    % Collect the index of the first occurrence in B for every A:
%    M = zeros(1, nA);
%    for iA = 1:nA
%       Ind = find(strcmp(A{iA}, B), 1);
%       if isempty(Ind) == 0
%          M(iA) = Ind(1);
%       end
%    end
% 
% else  % nB <= nA, B is smaller, so it is cheaper to loop over B:
%    % Mark every A which equal the current B
%    M = zeros(1, nA);
%    for iB = nB:-1:1
%       M(strcmp(B{iB}, A)) = iB;  % Same as M(find(strcmp()))
%    end
% end
% 
% AI = find(M);  % If any occurrence was found, this A exists...
% BI = M(AI);    % at this index in B
% end
