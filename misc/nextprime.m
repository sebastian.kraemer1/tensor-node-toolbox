function p = nextprime(n)
if (isprime(n))
    p=n;
else
    while(~isprime(n))
        n=n+1;
    end
    p=n;
end
end