function vol = data_volume(N)
% DATA_VOLUME  sums up the number of entries in the nodes of N
%
%   See also: TENSOR_NODE_NOTATION3_COMMON_REPRESENTATIONS.mlx

if ~isa(N,'cell')
    N = {N};
end
vol = 0;
for i = 1:length(N)
    vol = vol + numel(N{i}.data);
end
end