function [A,ncell,Pos,pos,resh,varargin] = unfold(N,varargin)
% UNFOLD  Unfold (reshape and permute) a node into a conventional 
% multiarray (vector, matrix or tensor)
%
%   A = UNFOLD(N,m_1,...,m_s) sorts N.data of a tensor node N according to 
%   the mode name subsets (cells) m_1 to m_s and returns a multiarray A of
%   dimension s, if m_1,...,m_s are all mode names of N (also in respect of
%   multiplicity) or s+1 where m_{s+1} contains the remaining
%   (multiplicities) of mode names.
%
%   [A,ncell,Pos] = UNFOLD(N,m_1,...,m_s) also returns a cell ncell
%   where each ncell{i} is a vector containing the single mode sizes of the 
%   mode names within m_i. Pos is a cell where Pos{i} contains the single
%   positions of the mode names within m_i in N.
%
%   ncell and Pos are mostly for use by subsequent functions.
%
%   If mode names appear multiple times, they are treated in same order of
%   appearance within N. 
%   
%   If mode_names contains all mode_names of N, then
%   [A,ncell,Pos] = UNFOLD(N,mode_names) and 
%   [n,nvec,pos] = node_size(N,mode_names) 
%   return the same result in the sense that A = N.data, ncell{1} = nvec 
%   and Pos{1} = pos.
%
%   [A,ncell,Pos] = UNFOLD(N,m_1,...,m_s,'permute',p) is equivalent to a 
%   posterior permutation of A, ncell and Pos.
%   This is only useful if multiple mode names appear, but are to be
%   treated in another order.
%
%   Examples:
%       N = init_node({'alpha','gamma','gamma'},[1,2,3]);
%       N.data(:) = 1:6;
%       [A,ncell,Pos] = unfold(N,{'alpha','gamma'});
%
%       % returns:
%       A % = [1,3,5; 2,4,6]
%       ncell{1} % = [1,2]
%       ncell{2} % = 3
%       Pos{1} % = [1,2]
%       Pos{2} % = 3
%
%   See also: TENSOR_NODE_NOTATION1_SINGLE_NODES.mlx, INIT_NODE, NODE_SIZE, TMUL, FOLD

k = nargin - 1; % = length(varargin)
if k > 2 && strcmp(varargin{k-1},'permute')
    perm = varargin{k};
    varargin = varargin(1:k-2);
else
    perm = [];
end 

for i = 1:length(varargin)
    if ~isa(varargin{i},'cell')
        varargin{i} = varargin(i);
    end
end

varconc = [varargin{:}];
if length(varconc) < length(N.mode_names)
    varargin = [varargin,{listdiff(N.mode_names,varconc)}];  
    [A,ncell,Pos,pos,resh] = unfold(N,varargin{:},'permute',perm);
return;
end
[~,npos,pos] = node_size(N,varconc);

if isempty(perm)
    perm = 1:length(varargin);
end    

n = zeros(1,length(varargin));
ncell = cell(1,length(varargin));
Pos = cell(1,length(varargin));

k = 1;
for i = 1:length(varargin)
    inc = length(varargin{i});
    Pos{i} = pos(k:k+inc-1);
    ncell{i} = npos(k:k+inc-1);
    k = k + inc;
end

pos = [Pos{perm}];
for i = 1:length(perm)
    n(i) = prod(ncell{perm(i)});
end
    
if length(pos) <= 1 % for vectors
    pos = [1,2];
end
resh = [n,1];
A = reshape( permute(N.data,pos) ,resh);

end