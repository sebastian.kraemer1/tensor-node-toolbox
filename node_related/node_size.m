function [n,nvec,pos] = node_size(N,mode_names)
% NODE_SIZE  Get the mode sizes of a nodee.
%
%   n = NODE_SIZE(N) returns a struct such that init_node(mode_names,n) has
%   the same size as N,
%
%   n = NODE_SIZE(N,mode_names) returns the same struct, but limited to the
%   mode names in mode_names (dublicate mode names in mode_names give the
%   same results).
%
%   [n,nvec,pos] = NODE_SIZE(N,mode_names) also returns a vector nvec with
%   the mode sizes in the order of mode_names and a vector pos with the
%   positions of these mode names in N.data . For nvec and pos, dublicate 
%   mode names in mode_names give another result.
%
%   If N is a cell, then nvec and pos will be cells as well. n then is a
%   merger of all structs n_i = NODE_SIZE(N{i}).
%
%   Example:
%       N = init_node({'alpha','gamma','gamma'},[4,5,6]);
%       
%       n = node_size(N,'gamma') % struct n with n.gamma = [5,6]
%       [n,nvec,pos] = node_size(N,{'gamma','alpha','gamma'});
%   
%       % returns:
%       n % struct with fields: gamma: [5,6], alpha: 4
%       nvec % = [5,4,6]
%       pos % = [2,1,3]
%
%   See also: TENSOR_NODE_NOTATION1_SINGLE_NODES.mlx, INIT_NODE, SIZE, UNFOLD

n = struct;

if isa(N,'cell')
    n = struct;
    nvec = cell(1,length(N));
    pos = cell(1,length(N));
    for i = 1:length(N)
        if nargin == 1
            [ds,nvec{i},pos{i}] = node_size(N{i});
        else
            [ds,nvec{i},pos{i}] = node_size(N{i},mode_names);
        end
        n = merge_fields(n,ds);
    end
    n = orderfields(n);
else
    if nargin == 1
        mode_names = N.mode_names;
    end
    if ~isa(mode_names,'cell')
        mode_names = {mode_names};
    end
    lmn = length(mode_names);
    nvec = zeros(1,lmn);
    pos = zeros(1,lmn);
    sizeN = size(N.data);
    sizeN = [sizeN,ones(1,length(N.mode_names)-length(sizeN))];
    
    count = struct;
    for i = 1:lmn
        count.(mode_names{i}) = 1;
    end
    for i = 1:lmn
        posall = N.pos.(mode_names{i});
        n.(mode_names{i}) = sizeN(posall);
        c = count.(mode_names{i});
        nvec(i) = sizeN(posall(c));
        pos(i) = posall(c);
        count.(mode_names{i}) = c + 1;
    end
end

end