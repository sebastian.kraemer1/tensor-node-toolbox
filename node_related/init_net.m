function N = init_net(m,n)
% INIT_NET  recursively calls init_node/init_net for every element in m
%
%   See also: TENSOR_NODE_NOTATION7_PLAIN_NETWORKS.mlx

N = cell(size(m));

for i = 1:numel(m)
    if isa(m{i},'cell')
        N{i} = init_node(m{i},n);
    else
        N{i} = init_net(m{i},n);
    end
end

% k = length(m);
% N = cell(1,k);
% for i = 1:k
%     N{i} = init_node(m{i},n);
% end