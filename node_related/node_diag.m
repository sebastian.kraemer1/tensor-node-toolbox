function diagN = node_diag(N,diag_mode_names)

if nargin == 1
    diag_mode_names = fieldnames(N.pos);
    for i = 1:length(diag_mode_names)
        if length(N.pos.(diag_mode_names{i})) == 1
            diag_mode_names{i} = {};
        end
    end
    diag_mode_names = [{},diag_mode_names{:}];
end

double_diag_mode_names = [diag_mode_names,diag_mode_names];
mode_name_remainder = listdiff(N.mode_names,double_diag_mode_names);
    
[A,ncell] = unfold(N,double_diag_mode_names,mode_name_remainder);

diag_mode_names_size = ncell{1}(1:length(diag_mode_names));
selector = eye(prod(diag_mode_names_size),'logical');
diagA = A(selector(:),:);

diagN = fold(diagA,[diag_mode_names,mode_name_remainder],[diag_mode_names_size,ncell{2}]);
