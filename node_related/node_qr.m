function [Q,R] = node_qr(N,Gamma)
% NODE_QR  Node-QR-decomposition
%
%   [Q,R] = NODE_QR(N,Gamma) returns the QR-decomposition of the node N
%   with respect to the mode names in the cell Gamma. The node Q is
%   Gamma-orthogonal and R has dublicate mode names (Gamma,Gamma).
%
%   If Q is less tall than wide, Gamma can not contain multiple mode names.
%
%   N may contain dublicate mode names and will start taking mode names in
%   Gamma from the right.
%
%   Example:
%       Alpha = mna('alpha',1:5);
%       N = init_node(Alpha,2*ones(1,5));
%       N = randomize_node(N);
%       Gamma = mna('alpha',[4,5]);
%       [Q,R] = node_qr(N,Gamma);
% 
%       T = boxtimes_part(Q,Q,str_setdiff(Alpha,Gamma));
%       unfold(T,Gamma,Gamma) % = eye(4)
%       unfold(R,Gamma,Gamma) % upper triangular
%
%   See also: NODE_SVD, UNFOLD, BOXTIMES_PART

if ~isa(Gamma,'cell')
    Gamma = {Gamma};
end

names1 = listdiff(N.mode_names,Gamma);

[A,n] = unfold(N,names1,Gamma);
[Q,R] = qr(A,0);

r = n{2};
if size(R,1) ~= size(R,2)
    %     warning('R from node_qr is not quadratic!');
    if length(n{2}) > 1
        error('Can not fold into multiple mode names when size is decreased!');
    end
    r = size(R,1);
end

Q = fold(Q,[names1,Gamma],[n{1},r]);
R = fold(R,[Gamma,Gamma],[r,n{2}]);

end