function N = matrix_node_pinv(N) %TODO
N.data = pinv(N.data);
N.mode_names = N.mode_names(end:-1:1);
if N.mode_names{1} ~= N.mode_names{2}
    N.pos.(N.mode_names{1}) = 1;
    N.pos.(N.mode_names{2}) = 2;
end
end