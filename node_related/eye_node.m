function N = eye_node(mns,n)

s = 1;
for i = 1:length(mns)
    s = s * n.(mns{i});
end

N = fold(eye(s),[mns,mns],n);

end