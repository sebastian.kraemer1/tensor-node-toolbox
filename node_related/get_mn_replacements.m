function [Z_unpacked,glob_repl,outer_mn_id,seed] = get_mn_replacements(Z,seed)
% GET_MN_REPLACEMENTS  Identifies a mode name replacement matrix to be able
% to unpack the nested cells in Z.
%
%   [Z_unpacked,glob_repl] = GET_MN_REPLACEMENTS(Z) takes a cell Z of which
%   each entry maybe such a cell again and unpacks all nested etries into a
%   single cell Z_unpacked. Each entry glob_repl(i,j) gives an index how to
%   rename Z_unpacked{i}.mode_names{j}, such that after renaming,
%   Z_unpacked produces the same multiplication network.
%
%   GET_MN_REPLACEMENTS does not yet replace mode names.
%
%   Examples:
%       m{1} = init_node({'a','b'},[1,2]);
%       m{2} = init_node({'b'},[3]);
%       m{3} = init_node({'b','b','c'},[3,2,4]);
%       [Z_unpacked,glob_repl] = GET_MN_REPLACEMENTS({m{1}, {m{2},m{3}} })
%       % returns
%
%       glob_repl = [4, 5, 0;
%                    1, 0, 0;
%                    1, 5, 6];
%       % Z_unpacked equals m
%
%   Also see: INIT_NODE, BOXTIMES

[Z,J,part_rule] = unpack(Z,1);

Z_unpacked = Z;
nZ_orig = length(Z);

% Brackets in original position
J = [J;1,1,length(Z)];
lvl_max = max(J(:,1));

% Brackets in transformed position
J_s = J(:,2:3);

% Replacements on certain level
repl = cell(lvl_max,1);
% Evaluated brackets
repr = cell(lvl_max,1);

if nargin == 1
    k = 0;
    seed = 0;
else
    k = seed;
end
orig_pos = 1:length(Z);

max_mode_l = 0;
for j = 1:length(Z)
    max_mode_l = max_mode_l+length(Z{j}.mode_names);
end

% Template for cycle_modes
outer = zeros(nZ_orig,max_mode_l);

for i = lvl_max:-1:1
    repr{i} = cell(nZ_orig,1);
    repl{i} = zeros(nZ_orig,max_mode_l);
       
    % Brackets on level i
    on_lvl = find(J(:,1)==i)';
    for j = on_lvl
        % Find result of multiplication within bracket
        [V,repl{i},k] = cycle_modes(Z,part_rule{j},repl{i},outer,J_s(j,1),J_s(j,2),k,orig_pos);

        V.orig_span = orig_pos(J_s(j,1):J_s(j,2));
               
        % Replace in Z
        Z{orig_pos(J_s(j,1))} = V;

        % Delete in original positions
        orig_pos(J_s(j,1)+1:J_s(j,2)) = [];        
        J_s(J_s>J_s(j,1)) = J_s(J_s>J_s(j,1)) - (J_s(j,2)-J_s(j,1));
        
        % Save for backtracking
        repr{i}{J(j,2)} = V;     
    end
    
end

% Backtrack global replacement scheme
glob_repl = zeros(nZ_orig,max_mode_l);
glob_repl = replace_rec(glob_repl,repl,repr,1,1,lvl_max);

% Reduce to minimal matrix size
glob_repl = glob_repl(:,sum(glob_repl,1)>0);

% Transform glob_repl to ascending order
glob_repl_tr = glob_repl';
non_zero_repl = glob_repl_tr(glob_repl_tr(:)~=0);
uni_repl = unique(non_zero_repl,'stable');
arr(uni_repl) = seed + (1:length(uni_repl));
glob_repl_tr_new = glob_repl_tr;
glob_repl_tr_new(glob_repl_tr(:)~=0) = arr(glob_repl_tr(glob_repl_tr(:)~=0));
glob_repl = glob_repl_tr_new';
repl{1}(repl{1}(:)~=0) = arr(repl{1}(repl{1}(:)~=0));

% outer mode names
if isfield(V,'mode_names_pos')
    outer_mn_id = zeros(1,length(V.mode_names_pos));
    for i = 1:length(V.mode_names_pos)
        outer_mn_id(i) = repl{1}(V.mode_names_pos{i}(1,1),V.mode_names_pos{i}(1,2));
    end
else
    outer_mn_id = [];
end

seed = max(glob_repl(:)) + 1;
% seed = k+1;


% outer_mn_pos = V.mode_names_pos;
% outer_mn_pos = outer_mn_rec(outer_mn_pos,repr,1,lvl_max)
% outer_mn = repr{1}{1}.mode_names;

end

% function outer_mn_rec(outer_mn_pos,repr,lvl,lvl_max)
% 
% for i = 1:length(outer_mn_pos)
%     for j = 1:size(outer_mn_pos{i},1)
%         if ~isempty(repr{lvl+1}{outer_mn_pos{i}(j,1)})
%             outer_mn_pos{i}(j,:) = repr{lvl+1}{outer_mn_pos{i}(j,1)}(
% 
% 
% end


function [Z,J,part_rule] = unpack(Z,lvl)
% Unpack nested cells into one cell and save nestedness scheme in J
% J(:,1) is the level, J(:,2) bracket begin, J(:,3) bracket end

Z = reshape(Z,[1,numel(Z)]);

i = 1;
J = zeros(0,3);
part_rule_tilde = cell(3,1);
part_rule = {};

while i <= length(Z)
    if isa(Z{i},'cell')
        [Ztilde,Jtilde,part_rule_add] = unpack(Z{i},lvl+1);
        Z = [Z(1:i-1),Ztilde,Z(i+1:end)]; % insert expension
        J = [J; Jtilde(:,1), Jtilde(:,2:3)+i-1; [lvl+1,i,i+length(Ztilde)-1]];
        i = i + length(Ztilde);
        part_rule = [part_rule;part_rule_add];
    elseif isa(Z{i},'char')
        % todo: might as well introduce ghost nodes. That would simplify a
        % lot
        % _\ : ghost node with correct modes sizes?
        ind = strcmp(Z{i},{'_\','_','^'}); 
        if ~isa(Z{i+1},'cell')
            Z{i+1} = Z(i+1);
        end
        part_rule_tilde{ind} = Z{i+1};
        Z([i,i+1]) = [];
    else
        i = i + 1;
    end
end
part_rule = [part_rule; {part_rule_tilde}];

end


function glob_repl = replace_rec(glob_repl,repl,repr,j,lvl,lvl_max)
% Backtrack replacements to higher levels

span = repr{lvl}{j}.orig_span;

for i = span
    if lvl < lvl_max && ~isempty(repr{lvl+1}{i})
        % Transfer replacements from lower level to outer modes of higher
        % level
        for k = 1:length(repr{lvl+1}{i}.mode_names_pos)
            S = repr{lvl+1}{i}.mode_names_pos{k};
            for i_s = 1:size(S,1)
                s = S(i_s,:);
                repl{lvl+1}(s(1),s(2)) = repl{lvl}(i,k);
            end
        end
        
        % Continue to higher level
        glob_repl = replace_rec(glob_repl,repl,repr,i,lvl+1,lvl_max);
    else
        % Node is not in a higher level cell
        glob_repl(i,:) = repl{lvl}(i,:);
    end    
end

end


function [V,repl,k] = cycle_modes(Z,part_rule,repl,outer,a,b,k,orig_pos)

mode_map = struct;
last_occ = struct;
keep = [];

for i = orig_pos(a:b)
    prev = struct;

    for j = 1:length(Z{i}.mode_names)
        outer(i,j) = 1;

        if any(strcmp(Z{i}.mode_names{j},part_rule{1})) ...
                || (~isempty(part_rule{2}) && ~any(strcmp(Z{i}.mode_names{j},part_rule{2})) && ~any(strcmp(Z{i}.mode_names{j},part_rule{3}))) ...
                || ~isfield(mode_map,Z{i}.mode_names{j})
            % New index for new mode name
            k = k + 1;
            mode_map.(Z{i}.mode_names{j}) = k;
        else        
            if isfield(prev,Z{i}.mode_names{j}) % any(strcmp(Z{i}.mode_names{j},Z{i}.mode_names(1:j-1)))
                % Check if a double mode names
                k = k + 1;
                mode_map.(Z{i}.mode_names{j}) = k;
            else
                % Found inner mode names, modify entry in outer
                if ~any(strcmp(Z{i}.mode_names{j},part_rule{3}))
                    outer(i,j) = 0;
                    s = last_occ.(Z{i}.mode_names{j});
                    outer(s(1),s(2)) = 0;
                else
                    outer(i,j) = 0;
                end
            end
        end
         
        % Save replacement
        repl(i,j) = mode_map.(Z{i}.mode_names{j}); 
        % Save last appearance for easier inner mode detection
        last_occ.(Z{i}.mode_names{j}) = [i,j];
        prev.(Z{i}.mode_names{j}) = 1;
    end
end

% Construct result of multiplication
[s1,s2] = find(outer);
S = [s1(:),s2(:)];

V = struct;
V.mode_names = cell(size(S,1),1);
V.mode_names_pos = cell(size(S,1),1);

for i = 1:size(S,1)
    V.mode_names{i} = Z{S(i,1)}.mode_names{S(i,2)};
    
    % Save "pointers" to outer mode names of original factors
    [s1,s2] = find(repl==repl(S(i,1),S(i,2)));
    V.mode_names_pos{i} = [s1,s2];
end

end


