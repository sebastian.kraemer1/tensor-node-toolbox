function N = node_transpose(N)
% NODE_TRANPOSE  transposes a node N
%
%   NODE_TRANPOSE(N) expects a tensor node N and will revert the order of
%   mode names. Dublicate mode names switch roles.
%
%   See also:
%   TENSOR_NODE_NOTATION4_ORTHOGONALITY_AND_DUPLICATE_MODE_NAMES.mlx,
%   NET_TRANSPOSE

nN = length(N.mode_names);

list = nN:-1:1;

N.mode_names = N.mode_names(list);
N.pos = node_calc_pos(N);
N.data = permute(N.data,list);