function [res_max, v_max, ne_max] = check_if_standard(fN,sigma,G)
% CHECK_IF_STANDARD  tests if fN and sigma form a standard representation
%
%   See also: TENSOR_NODE_NOTATION8_STANDARD_REPRESENTATIONS.mlx

if nargin <= 2
    G = net_derive_G(fN);
end

res_max = -Inf;
for v = 1:length(fN)
    for ne = neighbors(G,v)'
        H = fN{v};
        for j = findedge(G,v,setdiff(neighbors(G,v)',ne))'
            H = boxtimes_part(H,sigma{j},{},sigma{j}.mode_names);
        end
        gamma = intersect(fN{v}.mode_names,fN{ne}.mode_names);
        H = boxtimes_part(H,H,setdiff(H.mode_names,gamma));
        [~,si] = node_size(H,gamma{1});
        res = norm(H.data-eye(si))/sqrt(si(1));
        if res > res_max
            res_max = max(res,res_max);
            v_max = v; ne_max = ne;
        end
    end
end

if nargout == 0
    fprintf('Network is standardized with a tolerance of %e \n',res_max);
end

end