function N = boxtimes_part(N1,N2,contract,common)
% BOXTIMES_PART  Multiplies two nodes by partially specifying mode names.
%
%   N = BOXTIMES_PART(N1,N2,contract) sets common = {}. 
%   Since names1 and names2 can be calculated, the call is equivalent to 
%   N = tmul(N1,N2,names1,names2,contract,common).
%
%   The outcome N can have multiple mode names.
%
%   Example:
%       N1 = init_node({'alpha','beta','gamma'},[1,2,3]);
%       N2 = init_node({'beta','gamma','delta'},[2,3,4]);
%         
%       N = boxtimes_part(N1,N2,'beta')
%       % returns:
%       %   N = 
%       %
%       %    struct with fields:
% 
%       %      mode_names: {'alpha'  'gamma'  'gamma'  'delta'}
%       %             pos: [1×1 struct]
%       %            data: [1×3×3×4 double]
%
%   See also: BOXTIMES, INIT_NODE, TMUL

if nargin == 3
    common = cell(0);
end
inner_modes = [contract,common];
    
names1 = listdiff(N1.mode_names,inner_modes);
names2 = listdiff(N2.mode_names,inner_modes);

N = tmul(N1,N2,names1,names2,contract,common);

end