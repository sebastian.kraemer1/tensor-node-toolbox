function N = ones_node(mns,n)

s = 1;
for i = 1:length(mns)
    s = s * n.(mns{i});
end

N = fold(ones(s,1),mns,n);

end