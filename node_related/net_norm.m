function L = net_norm(N)

L = sqrt(get_data(boxtimes(N,N)));