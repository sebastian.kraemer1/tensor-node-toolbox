function N = fold(A,mode_names,n)
% FOLD  Inverse operation to unfold
%
%   N = FOLD(A,mode_names,mode_sizes) turns a (multi)array A into a tensor
%   node N with mode names according to mode_names and mode sizes according
%   to n. numel(A) needs to equal the product of all mode sizes.
%
%   Example:
%       N = init_node({'alpha','beta','gamma'},[1,2,3]);
%       N.data(:) = 1:6;
%       [A,ncell,Pos] = unfold(N,{'alpha','gamma'},'beta');
%       N = fold(A,{'alpha','beta','gamma'},...
%           [ncell{1}(1),ncell{2},ncell{1}(2)])
%       size(N.data) % = [1,2,3]
%       N.data(:) % = 1:6
%
%   See also: TENSOR_NODE_NOTATION1_SINGLE_NODES.mlx, UNFOLD, INIT_NODE

N = init_node(mode_names,n);
N.data(:) = A(:);

end