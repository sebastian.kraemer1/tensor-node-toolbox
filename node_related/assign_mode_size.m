function n = assign_mode_size(mode_names,mode_size)
% ASSIGN_MODE_SIZE  assignes mode_size(i) to n.(mode_names{i})
%
%   If mode name mode_names(i) has appeared before, then mode_size(i) is
%   added as additional entry to n.(mode_names{i}).
%
%   Use this to initialize nodes with preset mode sizes. 
%
%   Examples:
%       mode_names = mna('alpha',[1,2,3,4]);
%
%       n = assign_mode_size(mode_names,1:4);
%       % returns:
%       % n = 
%       %	 struct with fields:
% 
%       %    alpha_1: 1
%       %    alpha_2: 2
%       %    alpha_3: 3
%       %    alpha_4: 4
%         
%       n = assign_mode_size([mode_names,'alpha_3'],1:5)
%       % returns:
%       % n = 
%       %    struct with fields:
% 
%       %    alpha_1: 1
%       %    alpha_2: 2
%       %    alpha_3: [3,5]
%       %    alpha_4: 4
%
%   See also: TENSOR_NODE_NOTATION1_SINGLE_NODES.mlx, INIT_NODE, MNA

if numel(mode_size) == 1
    mode_size = mode_size*ones(1,numel(mode_names));
end

n = struct;
for i = 1:numel(mode_names)
    n.(mode_names{i}) = [];
end
for i = 1:numel(mode_names)
    n.(mode_names{i}) = [n.(mode_names{i}),mode_size(i)];
end
end