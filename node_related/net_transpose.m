function Net_T = net_transpose(Net)
% NET_TRANSPOSE  takes a cell and recursively transposes its elements
%
%   NET_TRANSPOSE(Net) expects a cell Net and will revert the order of
%   elements as well as order of mode names within each tensor node.
%
%   See also: TENSOR_NODE_NOTATION6_NESTED_BOXTIMES, NODE_TRANSPOSE

Net_T = cell(size(Net));
numetNet = numel(Net);

list = numetNet:-1:1;

for i = 1:numetNet
    if isa(Net{i},'cell')
        Net_T{list(i)} = net_transpose(Net{i});
    else
        Net_T{list(i)} = node_transpose(Net{i});
    end
end

%% this version allowed a variable number of inputs
% varargout = cell(size(varargin));
% 
% list = nargin:-1:1;
% for i = 1:numel(varargin)
%     if isa(varargin{i},'cell')
%         varargout{list(i)} = cell(1,numel(varargin{i}));
%         [varargout{list(i)}{:}] = net_transpose(varargin{i}{:});
%     else
%         varargout{list(i)} = node_transpose(varargin{i});
%     end
% end

