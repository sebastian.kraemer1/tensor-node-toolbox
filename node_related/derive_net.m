function varargout = derive_net(mn_repl_inv,varargin)
% DERIVE_NET  renames mode names without changing any values
%
%   DERIVE_NET(mn_repl_inv,N1,N2,...) replaces the mode names in N1,N2,...
%   as specified by the struct mn_repl_inv.
%
%   See also: LINSOLVE_EQUALNET

nout = length(varargin);
varargout = varargin;

for i = 1:nout
    mn_orig = varargin{i}.mode_names;
    orig_pos = varargin{i}.pos;
    varargout{i}.pos = struct;
    
    for j = 1:length(mn_orig)
        % rename modes specified:
        if isfield(mn_repl_inv,mn_orig{j})
            mn = mn_repl_inv.(mn_orig{j});
        else
            mn = mn_orig{j};
        end
        varargout{i}.mode_names{j} = mn;
        
        % pos field:
        if ~isfield(varargout{i}.pos,mn)
            varargout{i}.pos.(mn) = orig_pos.(mn_orig{j});      
        else
            varargout{i}.pos.(mn) = [varargout{i}.pos.(mn),orig_pos.(mn_orig{j})];      
        end
    end
end