function N = sort_node_mn(N,m)

if ~isequal(N.mode_names,m)
    A = unfold(N,m);
    
    N = fold(A,m,node_size(N));
end
end