function data = get_data(N)
% GET_DATA  returns the field data of the node N
%
%   See als: TENSOR_NODE_NOTATION5_NODE_QR_AND_NODE_SVD.mlx

data = N.data;