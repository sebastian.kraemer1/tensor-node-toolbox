function varargout = prime_node(priming_in,varargin)

nout = length(varargin);
varargout = varargin;

for i = 1:nout
    priming = priming_in;
    mn_N_i = varargin{i}.mode_names;
    orig_pos = varargin{i}.pos;
    varargout{i}.pos = struct;
    
    for j = 1:length(mn_N_i)
        % rename modes specified:
        if isfield(priming,mn_N_i{j})
            mn_prime = priming.(mn_N_i{j});
            priming.(mn_N_i{j}) = mn_N_i{j}; % change only first occurance
        else
            mn_prime = mn_N_i{j};
        end
        varargout{i}.mode_names{j} = mn_prime;
        
        % pos field:
        if ~isfield(varargout{i}.pos,mn_prime)
            varargout{i}.pos.(mn_prime) = orig_pos.(mn_N_i{j});      
        else
            varargout{i}.pos.(mn_prime) = [varargout{i}.pos.(mn_prime),orig_pos.(mn_N_i{j})];      
        end
    end
end