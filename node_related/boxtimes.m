function T = boxtimes(varargin)
% BOXTIMES  multiplies multiple tensor nodes according to their node names.
%
%   T = BOXTIMES(N1) returns the tensor node input N1
%   T = BOXTIMES(N1,N2) returns the node product of two tensor nodes
%   according to their mode names. This variant has least overhead.
%
%   T = BOXTIMES(N), where N is a cell containing tensor nodes, returns
%   their product [x](N_1,...,N_end). The multiplication is performed in a
%   greedy order, trying to minimize neccessary computational cost. This
%   variant involves a certain overhead.
%
%   T = BOXTIMES(N,M), for two cells N, M, returns the same tensor node as
%   BOXTIMES(BOXTIMES(N),BOXTIMES(M)), but uses a better multiplication
%   order. If for example N and M represent order d tensors, BOXTIMES will
%   avoid the construction of the full tensors.
%
%   T = BOXTIMES(Z) takes a cell Z of which each entry maybe such a cell
%   again and interprets nestedness as brackets within a multiplication.
%   This is only relevent when mode names appear more than twice.
%
%   Example:
%       N = cell(3,1);
%       M = cell(3,1);
%
%       mode_names_a = mna('a',[1,2,3]);
%       mode_names_b = mna('b',[1,2]);
%
%       n = assign_mode_size([mode_names_a,mode_names_b],[100,200,300,2,3]);
%
%       N{1} = init_node({'a_1','b_1'},n);
%       N{2} = init_node({'a_2','b_1','b_2'},n);
%       N{3} = init_node({'a_3','b_2'},n);
%
%       M{1} = init_node({'a_1','b_1'},n);
%       M{2} = init_node({'a_2','b_1','b_2'},n);
%       M{3} = init_node({'a_3','b_2'},n);
%
%       % (N,M) interpreted as vararginwork:
%       %  (N1) -b1- (N2) -b2- (N3)
%       %   |a1       |a2       |a3
%       %  (M1) -b1- (M2) -b2- (M3)
%
%       for i = 1:3
%           N{i} = randomize_node(N{i});
%           M{i} = randomize_node(M{i});
%       end
%
%       boxtimes(N,M)
%
%   See also: TMUL, BOXTIMES_PART, INIT_NODE, GET_MN_REPLACEMENTS

global BOXTIMES_MEM
% 
% if BOXTIMES_MEM.active
% deactivate_boxtimes_mem()
% T_wo_mem = boxtimes(varargin{:});
% activate_boxtimes_mem()
% end

boxtimes_active = exist('BOXTIMES_MEM','var') && isa(BOXTIMES_MEM,'struct') && isfield(BOXTIMES_MEM,'active') && BOXTIMES_MEM.active;
use_record = false;

if boxtimes_active  
    [Net,Net_hash_info] = simple_unpack(varargin);
    hash_code = net_hash(Net_hash_info);
    
    equal_hash_code_inds = find(BOXTIMES_MEM.HASHLIST==hash_code);
%     nehci = length(equal_hash_code_inds);
%     if nehci > 1
%         warning('%d entries have the same hash code',nehci);
%     end
        
    number_nodes = length(Net);

    for boxtimes_nr = equal_hash_code_inds
        [Perms,equivalent] = check_equivalence(Net_hash_info,BOXTIMES_MEM.mem{boxtimes_nr}.Zall,number_nodes);
        if equivalent
            use_record = true;
            BOXTIMES_ID_mem_bxtnr = BOXTIMES_MEM.mem{boxtimes_nr};
            break;
        end
    end
end

if ~use_record
    T_sort_perm = [];
    
    %% fast boxtimes for input without cells
    if length(varargin) == 1 && ~isa(varargin{1},'cell')
        BOXTIMES_ID_mem_bxtnr.tmul = {};
        T = varargin{1};
        
        % nothing to save in BOXTIMES_ID_mem_bxtnr
    elseif length(varargin) == 2 && ~isa(varargin{1},'cell') && ~isa(varargin{2},'cell')
        contract = str_intersect(unique(varargin{1}.mode_names),varargin{2}.mode_names);
        names1 = listdiff(varargin{1}.mode_names,contract);
        names2 = listdiff(varargin{2}.mode_names,contract);
        
        [T,BOXTIMES_ID_mem_bxtnr.tmul{1}] = tmul(varargin{1},varargin{2},names1,names2,contract,{});
        
        if boxtimes_active
            BOXTIMES_ID_mem_bxtnr.tmul{1}.ind1 = 1;
            BOXTIMES_ID_mem_bxtnr.tmul{1}.ind2 = 2;
            
            % could skip this, but this way its nicer
            BOXTIMES_ID_mem_bxtnr.tmul{1}.pos = T.pos;
            BOXTIMES_ID_mem_bxtnr.tmul{1}.mode_names = T.mode_names;
        end
    else
        
        %% contraction mode
        if length(varargin) > 1 && isequal(varargin{end-1},'mode')
            contraction_mode = varargin{end};
            varargin = varargin(1:end-2);
%             contraction_mode_input_or_default = {'mode',contraction_mode};
        else
            contraction_mode = 'greedy_use_xy';
        end
        ind = strfind(contraction_mode,'_optfac');
        if ~isempty(ind)
            str = contraction_mode(ind+7:end);
            str(str=='_') = ' ';
            optfac = sscanf(str,'%g',1);
            optfac = optfac(1);
            contraction_mode = contraction_mode(1:ind-1);
        else
            optfac = 1;
        end
        
        show_contraction_order = 0;
        switch contraction_mode
            case 'optimal_show'
                show_contraction_order = 1;
                contraction_mode = 'optimal';
            case 'greedy_use_xy_show'
                show_contraction_order = 1;
                contraction_mode = 'greedy_use_xy';
            case 'show'
                show_contraction_order = 1;
                contraction_mode = 'greedy_use_xy';
        end
        
        
        %% Get and possibly replace mode names
        outer_mn_id = [];
        [mode_name_replacement_necessary,n] = check_if_repl_nec(varargin); 
        
        if mode_name_replacement_necessary
            [varargin,glob_repl,outer_mn_id] = get_mn_replacements(varargin);
            [Net,n,mn_repl] = rename_mn(varargin,glob_repl);
        else
            Net = varargin;
            mn_repl = [];
        end
               
        %% establish corresponding edge map
        edgemap = corresponding_edgemap(Net);
        if ~isempty(edgemap)
            modes = fieldnames(edgemap)';
        else
            modes = {};
        end
        num_nodes = length(Net);
        
        %% forced outer nodes (only possible if repl) and ghost nodes
        if ~isempty(outer_mn_id)
            % add ghost node with out mode names
            num_nodes = num_nodes + 1;
            
            for i = 1:length(outer_mn_id)
                mn = mna('mu',outer_mn_id(i));
                edgemap.(mn{1}) = [edgemap.(mn{1});num_nodes];
            end
        end
        
        % ghost nodes:
        V_ghost = false(1,num_nodes);
        for i = 1:length(Net)
            if isfield(Net{i},'ghost')
                V_ghost(i) = 1;
            end
        end
        
        % turn artificial node into ghost node
        V_ghost(end) = V_ghost(end) | ~isempty(outer_mn_id);
        V_ghost = find(V_ghost);
        
        %% turn edgemap into graph
        lemodes = length(modes);
        G = struct;
        G.Edge_Size = zeros(1,lemodes);
        G.Edge = cell(1,lemodes);
        for i = 1:lemodes
            G.Edge_Size(i) = n.(modes{i});
            G.Edge{i} = edgemap.(modes{i});
        end
        
        %% plan the contraction order (according to mode)
        if length(Net) > 1
            [Is,Js,Name_Part,V] = graph_contraction3(G,num_nodes,V_ghost,contraction_mode,show_contraction_order,optfac);
        else
            Is = [];
            V = 1;
        end
        
        %% contract connected nodes
        BOXTIMES_ID_mem_bxtnr.tmul = cell(1,length(Is));
        
        for i = 1:length(Is)
            contract = modes(Name_Part{i}.remove);
            common = modes(Name_Part{i}.keep);
            names1 = modes(Name_Part{i}.namesI);
            names2 = modes(Name_Part{i}.namesJ);
            
            [Net{Is(i)},BOXTIMES_ID_mem_bxtnr.tmul{i}] = tmul(Net{Is(i)},Net{Js(i)},names1,names2,contract,common);
            
            if boxtimes_active
                BOXTIMES_ID_mem_bxtnr.tmul{i}.ind1 = Is(i);
                BOXTIMES_ID_mem_bxtnr.tmul{i}.ind2 = Js(i);
                
                % possibly adapt mode names to original
                if ~isempty(mn_repl)
                    BOXTIMES_ID_mem_bxtnr.tmul{i}.pos = struct;
                    BOXTIMES_ID_mem_bxtnr.tmul{i}.mode_names = cell(1,length(Net{Is(i)}.mode_names));
                    num_mnr = length(Net{Is(i)}.mode_names);
                                               
                    for i_mnr = 1:num_mnr
                        temp = mn_repl.(Net{Is(i)}.mode_names{i_mnr});
                        BOXTIMES_ID_mem_bxtnr.tmul{i}.pos = append_field_entry(BOXTIMES_ID_mem_bxtnr.tmul{i}.pos,temp,i_mnr);
%                         
%                         if isfield(BOXTIMES_ID_mem_bxtnr.tmul{i}.pos,temp)
%                             BOXTIMES_ID_mem_bxtnr.tmul{i}.pos.(temp) = [BOXTIMES_ID_mem_bxtnr.tmul{i}.pos.(temp),i_mnr];
%                         else
%                             BOXTIMES_ID_mem_bxtnr.tmul{i}.pos.(temp) = i_mnr;
%                         end
                        BOXTIMES_ID_mem_bxtnr.tmul{i}.mode_names{i_mnr} = temp;
                    end
                else
                    BOXTIMES_ID_mem_bxtnr.tmul{i}.pos = Net{Is(i)}.pos;
                    BOXTIMES_ID_mem_bxtnr.tmul{i}.mode_names = Net{Is(i)}.mode_names;
                end
            end
        end
        
        T = Net{V}; % V is now has size 1 (V equals last i)
        

        
        %% remove unique marking of mode names
        if mode_name_replacement_necessary && ~isempty(T.mode_names)
            % ensure (dublicate!) mode name will appear in same order as
            % original multiplication describes (sorts to mu_1,mu_2,...)
            if length(T.mode_names) > 1
                [~,T_sort_perm] =  sort(str2double(extractAfter_eig(T.mode_names,'_'))); % sort by number marker
                
                T.mode_names = T.mode_names(T_sort_perm);
                T.data = permute(T.data,T_sort_perm);
            end
            
            T.pos = struct;
            for i = 1:length(T.mode_names)
                temp = mn_repl.(T.mode_names{i});
                T.pos = append_field_entry(T.pos,temp,i);
%                 if isfield(T.pos,temp)
%                     T.pos.(temp) = [T.pos.(temp),i];
%                 else
%                     T.pos.(temp) = i;
%                 end
                T.mode_names{i} = temp;
            end
        end
        
    end
    
    if boxtimes_active
        if isempty(T_sort_perm)
            T_sort_perm = 1:numel(T.mode_names);
        end
        BOXTIMES_ID_mem_bxtnr.sort_perm = T_sort_perm;
        BOXTIMES_ID_mem_bxtnr.Zall = Net_hash_info;
        
%         for i = 1:length(varargin)
%             BOXTIMES_ID_mem_bxtnr.input{i}.mode_names = varargin{i}.mode_names;
%             BOXTIMES_ID_mem_bxtnr.input{i}.size = size(varargin{i}.data);
%         end
        
%         BOXTIMES_ID_mem_bxtnr.output.mode_names = T.mode_names;        
        
        BOXTIMES_MEM.hash_fill_nr = BOXTIMES_MEM.hash_fill_nr + 1;
        if BOXTIMES_MEM.hash_fill_nr > length(BOXTIMES_MEM.HASHLIST)
            BOXTIMES_MEM.hash_fill_nr = 1;
        end
        
        BOXTIMES_MEM.mem{BOXTIMES_MEM.hash_fill_nr} = BOXTIMES_ID_mem_bxtnr;
        BOXTIMES_MEM.HASHLIST(BOXTIMES_MEM.hash_fill_nr) = hash_code;
    end
    
else
    BOXTIMES_ID_mem_bxtnr_perm = BOXTIMES_ID_mem_bxtnr;
    
    i = 1; % in case the boxtimes multiplication contains only a single node

    for k = 1:length(BOXTIMES_ID_mem_bxtnr.tmul)
        i = BOXTIMES_ID_mem_bxtnr.tmul{k}.ind1;
        j = BOXTIMES_ID_mem_bxtnr.tmul{k}.ind2;
               
        if length(Perms{i}) > 1
            BOXTIMES_ID_mem_bxtnr_perm.tmul{k}.pos1 = Perms{i}(BOXTIMES_ID_mem_bxtnr_perm.tmul{k}.pos1);
        end
        if length(Perms{j}) > 1
            BOXTIMES_ID_mem_bxtnr_perm.tmul{k}.pos2 = Perms{j}(BOXTIMES_ID_mem_bxtnr_perm.tmul{k}.pos2);
        end
        
        Net{i} = mem_tmul(Net{i},Net{j},BOXTIMES_ID_mem_bxtnr_perm.tmul{k});
        
        Perms{i} = {};
    end
    
    T = Net{i};
    
    if length(T.mode_names) > 1
        % sort result as in original multiplication
        T_sort_perm = BOXTIMES_ID_mem_bxtnr.sort_perm;
        
        T_sort_perm_inv(T_sort_perm) = 1:length(T.mode_names);
        pos_fieldnames = fieldnames(T.pos);
        for i = 1:length(pos_fieldnames)
            T.pos.(pos_fieldnames{i}) = T_sort_perm_inv(T.pos.(pos_fieldnames{i})); % TODO BUG $!$
        end
        
        T.mode_names = T.mode_names(T_sort_perm);
        T.data = permute(T.data,T_sort_perm);
    end
end

end

function [repl,n] = check_if_repl_nec(Z)
n = struct;
repl = true;
for i = 1:length(Z)
    if isa(Z{i},'cell') || isa(Z{i},'char')
        return;
    else
        s = size(Z{i}.data);
        s = [s,ones(1,length(Z{i}.mode_names)-length(s))];
        for j = 1:length(Z{i}.mode_names)
            if length(Z{i}.pos.(Z{i}.mode_names{j})) > 1
                return;
            end
            n.(Z{i}.mode_names{j}) = s(j);
        end
    end
end
repl = false;
end

% function [Z,Zchars] = simple_unpack(Z,Zchars)
% % Unpack nested cells into one cell and save nestedness scheme in J
% % J(:,1) is the level, J(:,2) bracket begin, J(:,3) bracket end
% 
% Z = reshape(Z,[1,length(Z)]);
% 
% i = 1;
% 
% while i <= length(Z)
%     if isa(Z{i},'cell')
%         [Ztilde,Ztildechars] = simple_unpack(Z{i},{});
%         Z = [Z(1:i-1),Ztilde,Z(i+1:end)]; % insert expension
%         Zchars = [Zchars,Ztildechars];
%         i = i + length(Ztilde);
%     elseif isa(Z{i},'char')
%         Zchars = [Zchars,Z([i,i+1])];
%         Z([i,i+1]) = [];
%     else
%         i = i + 1;
%     end
% end
% 
% end


