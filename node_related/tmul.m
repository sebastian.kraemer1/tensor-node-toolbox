function [N,Idj_k] = tmul(N1,N2,names1,names2,contract,common)
% TMUL  Multiplies two nodes by manually specifying mode names.
%
%   N = TMUL(N1,N2,names1,names2,contract,common) returns a tensor node 
%   with mode names [names1,common,names2] with 
%   N(names1,common,names2) 
%    = sum_contract N1(names1,common,contract)*N2(names2,common,contract)
%
%   Multiple mode names are not recommended, but if present, are treated in
%   order names1,common,contract for N1 and contract,common,names2 for N2.
%
%   TMUL uses multiprod written by Paolo de Leva (available on 
%   mathworks.com) for faster multiplication of arrays of matrices to
%   perform the core multiplication.
%
%   Example:
%       N1 = init_node({'alpha','beta','gamma'},[1,2,3]);
%       N2 = init_node({'beta','gamma','delta'},[2,3,4]);
%       N1 = randomize_node(N1);
%       N2 = randomize_node(N2);
%         
%       names1 = {'alpha'};
%       names2 = {'delta'};
%       common = {'beta'};
%       contract = {'gamma'};
%       N = tmul(N1,N2,names1,names2,contract,common);
%         
%       N
%       % returns
%           struct with fields:
% 
%             mode_names: {'alpha'  'beta'  'delta'}
%                   data: [1×2×4 double]
%                    pos: [1×1 struct]
%       
%   Example:
%       N1 = init_node({'alpha','beta','beta'},[1,2,3]);
%       N2 = init_node({'beta','beta','gamma'},[3,2,4]);
%       N1 = randomize_node(N1);
%       N2 = randomize_node(N2);
%       
%       names1 = {'alpha'};
%       names2 = {'gamma'};
%       common = {'beta'};
%       contract = {'beta'};
%       N = tmul(N1,N2,names1,names2,contract,common);
%         
%       N
%       % returns
%           struct with fields:
% 
%             mode_names: {'alpha'  'beta'  'gamma'}
%                   data: [1×2×4 double]
%                    pos: [1×1 struct]
%
%
%   See also: UNFOLD, FOLD    

[A1,n1,~,pos1,resh1] = unfold(N1,names1,common,contract,'permute',[1,3,2]);
[A2,n2,~,pos2,resh2] = unfold(N2,contract,common,names2,'permute',[1,3,2]);

if isempty(common)
    temp = A1*A2;
    N = fold(temp,[names1,names2],[n1{1},n2{3}]);   
else
    temp = multiprod(A1,A2,[1 2]);
    temp = permute(temp,[1,3,2]);
    N = fold(temp,[names1,common,names2],[n1{1},n1{2},n2{3}]);
end

% for mem_boxtimes:
Idj_k = struct;
Idj_k.pos1 = pos1;
Idj_k.pos2 = pos2;
Idj_k.resh1 = resh1;
Idj_k.resh2 = resh2;

Idj_k.fold = [n1{1},n1{2},n2{3}];
Idj_k.fold = [Idj_k.fold,ones(1,max(0,2-length(Idj_k.fold)))];

end