function N = matrix_node_diag(N)
% MATRIX_NODE_DIAG  takes a vector node and turns it into a diagonal matrix
%
%   MATRIX_NODE_DIAG(N) expects N do only have a single mode
%
%   See also: TENSOR_NODE_NOTATION5_NODE_QR_AND_NODE_SVD.mlx

gamma = N.mode_names{1};
N.data = diag(N.data);
N.pos.(gamma) = [1,2];
N.mode_names = {gamma,gamma};

end