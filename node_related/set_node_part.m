function N = set_node_part(N,A,varargin)

mode_names = [{},varargin{1:2:end}];
selection = [{},varargin{2:2:end}];

%% method 1 (order as specified + remainder)
% [A,ncell,~,~,~,names] = unfold(N,mode_names{:});
% A = A(selection{:},:);
% s = [size(A),ones(1,length(mode_names))];
% s = [s(1:length(mode_names)),ncell{end}];
% N = fold(A,[names{:}],s);

%% method 2 (order as original)
ld = listdiff(N.mode_names,mode_names);
[n,nvec,pos] = node_size(N,[mode_names,ld]);

d = length(N.mode_names);
s = length(mode_names);
sel = cell(1,d);
for i = s+1:d
    sel{pos(i)} = 1:nvec(i);
end
sel(pos(1:s)) = selection;

pos_inv = 1:length(pos); pos_inv(pos) = pos_inv;
if numel(A) == 1
    N.data(sel{:}) = A;
else
    si = ones(1,d);
    for i = 1:d
        si(i) = length(sel{i});
    end
    
    N.data(sel{:}) = reshape(permute(A,pos_inv),si);
end





% 
% n
% nvec
% pos
% 