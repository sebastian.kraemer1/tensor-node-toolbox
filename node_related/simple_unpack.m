% function [Z,Zhashrel] = simple_unpack(Z,Zhashrel)
% % Unpack nested cells into one cell and save nestedness scheme in J
% % J(:,1) is the level, J(:,2) bracket begin, J(:,3) bracket end
% 
% Z = reshape(Z,[1,length(Z)]);
% 
% i = 1;
% 
% while i <= length(Z)
%     if isa(Z{i},'cell')
%         [Ztilde,Ztildeall] = simple_unpack(Z{i},{});
%         Z = [Z(1:i-1),Ztilde,Z(i+1:end)]; % insert expension
%         Zhashrel = [Zhashrel,'{',Ztildeall,'}'];
%         i = i + length(Ztilde);
%     elseif isa(Z{i},'char')
%         Zhashrel = [Zhashrel,Z([i,i+1])];
%         Z([i,i+1]) = [];
%     else
%         s = size(Z{i}.data);
%         lmn = length(Z{i}.mode_names);
%         Zhashrel = [{},Zhashrel,{[Z{i}.mode_names;num2cell(s(1:lmn))]}];
% % Zhashrel = [{},Zhashrel,'x'];
%         i = i + 1;
%     end
% end
% 
% end

function [Net,Net_hash_info] = simple_unpack(Z)

neleZ = nested_cell_length(Z);

Net = cell(1,neleZ);
Net_hash_info = cell(1,neleZ);

[Net,i_N,Net_hash_info,i_Nhi] = simple_unpack_rec(Z,Net,0,Net_hash_info,0);
    
Net = Net(1:i_N);
Net_hash_info = Net_hash_info(1:i_Nhi);

end

function [Net,i_N,Net_hash_info,i_Nhi] = simple_unpack_rec(Z,Net,i_N,Net_hash_info,i_Nhi)

i = 1;

while i <= length(Z)
    if isa(Z{i},'cell')
        i_Nhi = i_Nhi + 1; Net_hash_info{i_Nhi} = '{';
        [Net,i_N,Net_hash_info,i_Nhi] = simple_unpack_rec(Z{i},Net,i_N,Net_hash_info,i_Nhi);
        i_Nhi = i_Nhi + 1; Net_hash_info{i_Nhi} = '}';
    elseif isa(Z{i},'char')
        i_Nhi = i_Nhi + 1; Net_hash_info{i_Nhi} = Z{i};
        i = i + 1;     
        
        i_Nhi = i_Nhi + 1; 
        if isempty(Z{i})
            % this means an empty set of mode names was entered as multiplication specification
            % using {' '} instead of {} denotes that this was not an empty cell of nodes
            Net_hash_info{i_Nhi} = {' '}; 
        else
            Net_hash_info{i_Nhi} = Z{i};
        end
    else
        s = size(Z{i}.data);
        lmn = length(Z{i}.mode_names); s_ = [s,ones(1,lmn-length(s))];
        i_N = i_N + 1; Net{i_N} = Z{i};
        i_Nhi = i_Nhi + 1; Net_hash_info{i_Nhi} = {Z{i}.mode_names;s_(1:lmn)};
    end
    i = i + 1;
end
    
end

function s = nested_cell_length(C)

if isa(C,'cell')
    s = 0;
    for i = 1:numel(C)
        s = s + nested_cell_length(C{i});
    end
else
    s = 1;
end

end

% function [Z,Zhashrel] = simple_unpack_rec2(Z,Zhashrel)
% 
% Z = reshape(Z,[1,length(Z)]);
% 
% i = 1;
% 
% while i <= length(Z)
%     if isa(Z{i},'cell')
%         [Ztilde,Ztildeall] = simple_unpack_rec(Z{i},{});
%         Z = [Z(1:i-1),Ztilde,Z(i+1:end)]; % insert expension
%         Zhashrel = [Zhashrel,'{',Ztildeall,'}'];
%         i = i + length(Ztilde);
%     elseif isa(Z{i},'char')
%         Zhashrel = [Zhashrel,Z([i,i+1])];
%         Z([i,i+1]) = [];
%     else
%         s = size(Z{i}.data);
%         lmn = length(Z{i}.mode_names);
%         Zhashrel = [{},Zhashrel,{[Z{i}.mode_names;num2cell(s(1:lmn))]}];
%         i = i + 1;
%     end
% end
% 
% end