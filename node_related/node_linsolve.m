function x = node_linsolve(A,b)

gamma = str_intersect(A.mode_names,b.mode_names);
names1 = listdiff(A.mode_names,gamma);
names2 = listdiff(b.mode_names,gamma);

[Aunf,nA] = unfold(A,gamma,names1);
[bunf,nb] = unfold(b,gamma,names2);

xunf = Aunf\bunf;
x = fold(xunf,[names1,names2],[nA{2},nb{2}]);