function N = mem_tmul(N1,N2,Idjtmulk)

A1 = reshape(permute(N1.data,Idjtmulk.pos1),Idjtmulk.resh1);
A2 = reshape(permute(N2.data,Idjtmulk.pos2),Idjtmulk.resh2);

if size(A1,3) == 1
    temp = A1*A2; 
else
    temp = multiprod(A1,A2,[1 2]);
    temp = permute(temp,[1,3,2]);
end

N.mode_names = Idjtmulk.mode_names;
N.pos = Idjtmulk.pos;
N.data = reshape(temp,Idjtmulk.fold);

end