function pos = node_calc_pos(N)

pos = struct;
for i = 1:length(N.mode_names)
    temp = N.mode_names{i};
    
    if isfield(pos,temp)
        pos.(temp) = [pos.(temp),i];
    else
        pos.(temp) = i;
    end

end