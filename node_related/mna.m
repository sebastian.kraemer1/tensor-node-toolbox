function m = mna(letter,range)
% MNA  Create an array of mode names
%
%   Examples:
%      MNA('alpha',[1,2]) % returns the cell {'alpha_1','alpha_2'}
%
%   See also: TENSOR_NODE_NOTATION1_SINGLE_NODES.mlx

m = cell(1,length(range));
for i = 1:length(range)
    m{i} = sprintf('%s_%d',letter,range(i));
end
end