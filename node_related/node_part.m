function N = node_part(N,varargin)
% NODE_PART  Selects a sub-tensor-node 
%
%   NODE_PART(N,mn_1,i_1,mn_2,i_2,...) returns the sub-tensor-node in which
%   mode names mn_j has been restricted to indices i_j. Each mn_j and i_j
%   can be cells of same size.
%
%   All non specified mode names are not restricted.
%
%   Dublicate mode names are restricted in order left to right.
%
%   See also: TENSOR_NODE_NOTATION1_SINGLE_NODES.mlx

if isa(N,'cell')
    for i = 1:length(N)
        N{i} = node_part(N{i},varargin{:});
    end
    
    return;
end

mode_names = [{},varargin{1:2:end}];
selection = [{},varargin{2:2:end}];

%% method 1 (order as specified + remainder)
% [A,ncell,~,~,~,names] = unfold(N,mode_names{:});
% A = A(selection{:},:);
% s = [size(A),ones(1,length(mode_names))];
% s = [s(1:length(mode_names)),ncell{end}];
% N = fold(A,[names{:}],s);

%% method 2 (order as original)
[ld,~,ind_contained] = listdiff(N.mode_names,mode_names);

[~,nvec,pos] = node_size(N,[mode_names(ind_contained),ld]);

d = length(N.mode_names);
s = length(mode_names(ind_contained));
sel = cell(1,d);
for i = s+1:d
    sel{pos(i)} = 1:nvec(i);
end
sel(pos(1:s)) = selection(ind_contained);
N.data = N.data(sel{:});
    