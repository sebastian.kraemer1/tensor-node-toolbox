function N = init_node(mode_names,n)
% INIT_NODE  Initializes an all zero node specified through mode_names
%
%   The mode sizes are taken from n. If a mode name appears again,
%   then the next entry in n.(mode_names{i}) is taken. 
%
%   If n is a double vector, then n = assign_mode_size(mode_names,n) is
%   called first.
%
%   Examples:
%       n = assign_mode_size(mna('alpha',[1,2,3,4,2]),1:5);     
%         
%       N = init_node(mna('alpha',[1,2,4,2]),n);
%       N % = struct with fields:
%         % 
%         %   alpha_1: 1
%         %   alpha_2: [2 4]
%         %   alpha_4: 3
% 
%       N.pos % = [1 2 4 5]
%       
%       size(N.data) % = [1,2,4,5]
% 
%       N = init_node(mna('alpha',[1,2,4,2]),[1,2,4,5]) % same N
%
%   See also: TENSOR_NODE_NOTATION1_SINGLE_NODES.mlx, ASSIGN_MODE_SIZE, RANDOMIZE_NODE

if ~isa(mode_names,'cell')
    mode_names = {mode_names};
end
if isa(n,'double')
    n = assign_mode_size(mode_names,n);
end


N = struct;
N.mode_names = mode_names;
N.pos = struct;

ln = zeros(1,length(mode_names));
for i = 1:length(mode_names)
    N.pos.(mode_names{i}) = [];
end
for i = 1:length(mode_names)
    N.pos.(mode_names{i}) = [N.pos.(mode_names{i}),i];
    ln(i) = n.(mode_names{i})(mod(length(N.pos.(mode_names{i}))-1,length(n.(mode_names{i})))+1);
end
    
N.data = zeros([ln,1]);

end