function N = free_add_scale(varargin)    % s1,N1,s2,N2)
% ADD_SCALE  calculates linear combinations of nodes with UNequal mode names
%
%   ADD_SCALE(s1,N1,s2,N2,s3,N3,...) will compute s1*N1 + s2*N2 + s3*N3 +
%   ...
%
%   ADD_SCALE(N1,s2,N2,N3,...) will compute N1 + s2*N2 + N3 + ...
%
%   See also: UNFOLD, FOLD, ADD_SCALE
    
error('TODO');