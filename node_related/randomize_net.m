function Net = randomize_net(Net)
% RANDOMIZE_NET  randomizes the entries of all given nodes
%
%   RANDOMIZE_NET(Net) takes a cell Net which itself may recursively
%   contain such cells or tensor nodes and randomizes of all such, keeping
%   the given structure.
%
%   See also: TENSOR_NODE_NOTATION3_COMMON_REPRESENTATIONS.mlx,
%   RANDOMIZE_NODE

for i = 1:numel(Net)
    if isa(Net{i},'cell')
        Net{i} = randomize_net(Net{i});
    else
        Net{i} = randomize_node(Net{i});
    end
end

end