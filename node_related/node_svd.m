function [U,s,Vt,r] = node_svd(N,names1,gamma,tol)
% NODE_SVD  Node-Singular-Value-Decomposition
%
%   [U,s,Vt,r] = NODE_SVD(N,names1,gamma) returns the SVD of the tensor node
%   N such that N = boxtimes(U,s,Vt) with mode names
%   U(names1,gamma), s(gamma), Vt(gamma,listdiff(N.mode_names,names1))
%   r equals the mode size of the new mode name gamma.
%
%   [U,s,Vt,r] = NODE_SVD(N,names1,gamma,tol) will use a relative truncation
%   tolerance tol (sigma_i/norm(sigma)>tol).
%
%   Example: 
%       Alpha = mna('alpha',1:3);
%       N = init_node(Alpha,2*ones(1,5));
%       N = randomize_node(N);
%         
%       [U,s,Vt] = node_svd(N,mna('alpha',1:2),'beta');
% 
%       T = boxtimes(U,s,Vt);
%       s.data
%       unfold(T,T.mode_names)-unfold(N,T.mode_names)
%
%   See also: NODE_QR, BOXTIMES

if ~isa(names1,'cell')
    names1 = {names1};
end

if nargin == 3
    tol = 1e-14;
end

names2 = listdiff(N.mode_names,names1);

[A,n] = unfold(N,names1,names2);
[U,S,V] = svd(A,'econ'); % previously ,0)
if min(size(S))~=1
    s = diag(S);
    r = max(1,sum(s/norm(s)>=tol));
else
    s = S(1);
    r = 1;
end

% if r < min(prod(n{1}),prod(n{2}))
%     warning('Rank is truncated automatically since A does not have full rank');
% end

U = fold(U(:,1:r),[names1,gamma],[n{1},r]);
s = fold(s(1:r),gamma,r);
Vt = fold(V(:,1:r)',[gamma,names2],[r,n{2}]);

end