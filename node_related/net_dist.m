function [dist,rel,reldist] = net_dist(A,B)
% NET_DIST  calculate the euclidean distance between A and B
%
%   NET_DIST(A,B) takes two tensor nodes (or networks) A and B with equal 
%   outer mode names and calculates the euclidean distance between. No
%   outer mode name may appear appear twice in the same node.
%
%   See also: TENSOR_NODE_NOTATION7_PLAIN_NETWORKS.mlx

ATA = boxtimes(A,A);
ATB = boxtimes(A,B);
BTB = boxtimes(B,B);

dist = sqrt(abs(ATA.data - 2*ATB.data + BTB.data));
rel = sqrt(BTB.data);
reldist = dist/rel;