function Net = ghost(Net,nr)
% GHOST  assigns the ghost field entry to every node found in Net
%
%   See also: TENSOR_NODE_NOTATION2_BOXTIMES.mlx, LINSOLVE

if nargin == 1
    if ~isa(Net,'cell')
        Net.ghost = 1;
    else
        
        % recursively
        for i = 1:numel(Net)
            if isa(Net{i},'cell')
                Net{i} = ghost(Net{i});
            else
                Net{i}.ghost = 1;
            end
        end
        
    end
else
    for i = nr
        Net{i}.ghost = 1;
    end
end

end

% if nargin == 1
%     if ~isa(N,'cell')
%         N.ghost = 1;
%     else
%         for i = 1:length(N)
%             N{i}.ghost = 1;
%         end
%     end
% else
%     for i = nr
%         N{i}.ghost = 1;
%     end
% end