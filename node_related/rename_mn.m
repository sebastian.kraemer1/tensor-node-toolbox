function [Net,n,mn_repl,mn_repl_inv] = rename_mn(Net,glob_repl,outer_mn_id)

if nargin <= 2
    outer_mn_id = [];
end

n = struct;
mn_repl = struct;
mn_repl_inv = struct;

for i = 1:length(Net)
    orig_mn = Net{i}.mode_names;
    s = size(Net{i}.data);
    s = [s,ones(1,length(orig_mn)-length(s))];
    nmn = length(orig_mn);
    
    Net{i}.pos = struct;
    
    for j = 1:nmn
        
        if ~ismember(glob_repl(i,j),outer_mn_id)           
            Net{i}.mode_names(j) = mna('mu',glob_repl(i,j));
            mn_repl.(Net{i}.mode_names{j}) = orig_mn{j};
            mn_repl_inv.(orig_mn{j}) = Net{i}.mode_names{j};
        end
        
        Net{i}.pos.(Net{i}.mode_names{j}) = j;
        n.(Net{i}.mode_names{j}) = s(j);
        
    end
    
end

end