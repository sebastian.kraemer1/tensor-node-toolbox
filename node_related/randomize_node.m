function N = randomize_node(N)
% RANDOMIZE_NODE Fill in entries with normed, gaussian entries.
%
%   Examples: 
%       n = assign_mode_size(mna('alpha',[1,2,3,4,2]),1:5);
% 
%       N = init_node(mna('alpha',[1,2,4,2]),n); 
%       N = randomize_node(N);
%       norm(N.data(:)) % = 1
%
%   See also: TENSOR_NODE_NOTATION2_BOXTIMES.mlx, INIT_NODE, RANDN

N.data = randn(size(N.data));
N.data = N.data/norm(N.data(:),'fro');

end