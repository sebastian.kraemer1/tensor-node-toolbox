function N = combine_modes(N,varargin)

v = varargin;

unf = v(1:2:end);
to = v(2:2:end);

[A,ncell] = unfold(N,unf{:});
    
remainder_names = listdiff(N.mode_names,[unf{:}]);

s = zeros(1,length(ncell)-1);
for i = 1:length(ncell)-1
    s(i) = prod(ncell{i});
end

N = fold(A,[{},to{:},remainder_names{:}],[s,ncell{end}]);