function N = add_scale(varargin)    % s1,N1,s2,N2)
% ADD_SCALE  calculates linear combinations of nodes with equal mode names
%
%   ADD_SCALE(s1,N1,s2,N2,s3,N3,...) will compute s1*N1 + s2*N2 + s3*N3 +
%   ...
%
%   ADD_SCALE(N1,s2,N2,N3,...) will compute N1 + s2*N2 + N3 + ...
%
%   See also: UNFOLD, FOLD
    
if isa(varargin{1},'struct')
    mn = varargin{1}.mode_names;
    [temp,ncell] = unfold(varargin{1},mn);
    i = 2;
else
    mn = varargin{2}.mode_names;
    [temp,ncell] = unfold(varargin{2},mn);
    temp = varargin{1}*temp;
    i = 3;
end

while i <= nargin
    if isa(varargin{i},'struct')
        temp = temp+unfold(varargin{i},mn);
        i = i + 1;
    else
        temp = temp+varargin{i}*unfold(varargin{i+1},mn);
        i = i + 2;
    end
end

N = fold(temp,mn,ncell{1});